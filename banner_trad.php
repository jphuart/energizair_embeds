<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php
// error_reporting(E_ALL);
// ini_set('display_errors','on');
$xmlTrad = file_get_contents ( 'traductions_new.xml' );
$xml = simplexml_load_string ( $xmlTrad );
$langUsed = $_GET ['languages'] != '' ? $_GET ['languages'] : "en";
$langUsedTowns = $_GET ['lang'] != '' ? $_GET ['lang'] : "en";

?>
<title><?php echo $xml->{$langUsed}->title;?></title>
<link rel="stylesheet" href="gblCustomFields.css" />
<style>
* {
	font-family: Verdana, Geneva, sans-serif;
}

html, body {
	margin: 0;
	text-align: center;
}

body {
	font-size: 14px;
	background-image: url(banners-content/background.jpg);
	background-repeat: no-repeat;
	background-position: top left;
	background-attachment: fixed;
}

input {
	height: 28px;
	line-height: 28px;
}

select {
	height: 28px;
	line-height: 28px;
}

option {
	height: 28px;
	line-height: 28px;
}

#town {
	width: 200px;
}

#town input {
	float: left;
}

#town span {
	hieght: 18px;
	line-height: 18px;
	display: block;
	float: left;
	font-size: 14px;
	margin-left: 13px;
	margin-bottom: 5px;
}

.box {
	background-image: url(banners-content/selection-cadre.png);
	width: 19px;
	height: 19px;
	border: 0;
	float: left;
}

.boxon {
	background-image: url(banners-content/selection-cadre-on.png);
	background-repeat: no-repeat;
	width: 19px;
	height: 19px;
	border: 0;
	float: left;
}

.boxhidder {
	display: none;
}

#embed {
	width: 313px;
	height: 283px;
	background-image: url(banners-content/selection-bkg.png);
	background-repeat: no-repeat;
	border: none;
	padding: 5px;
}

.cb120240, .cb120600, .cb468060, .cb160600, .cb655090, .cb728090 {
	padding-left: 28px !important;
	width: 154px !important;
}

.cb120240 {
	background-image: url(banners-content/120240picto.png);
	background-repeat: no-repeat;
}

.cb120600 {
	background-image: url(banners-content/120600picto.png);
	background-repeat: no-repeat;
}

.cb160600 {
	background-image: url(banners-content/160600picto.png);
	background-repeat: no-repeat;
}

.cb468060 {
	background-image: url(banners-content/486060picto.png);
	background-repeat: no-repeat;
}

.cb655090 {
	background-image: url(banners-content/655090picto.png);
	background-repeat: no-repeat;
}

.cb728090 {
	background-image: url(banners-content/728090picto.png);
	background-repeat: no-repeat;
}
</style>
<script type="text/javascript" src="jquery.js"></script>
<script type="text/javascript" src="jquery.carouFredSel-6.2.1-packed.js"></script>

<script>

Array.prototype.inArray = function(p_val) {
    var l = this.length;
    for(var i = 0; i < l; i++) {
        if(this[i] == p_val) {
            return true;
        }
    }
    return false;
}

var defaultTown='<?php if (isset($_GET['town'])) echo implode(',',$_GET['town']); ?>';
var defaultBanner='<?php if (isset($_GET['banner'])) echo $_GET['banner'];?>';
var defaultLang='<?php if (isset($_GET['lang'])) echo $_GET['lang'];?>';

var defaultTechnos='<?php if (isset($_GET['technos'])) echo $_GET['technos'];?>';

var xmlresult='';
var xmlbanner='';
var pendingResult=false;
var towns=[];
var banners=[];
var bannersValue=[];
var langs=[];
var checkedTowns='';

function updateTown() {
  pendingResult=true;	
  $.ajax({
  type: "GET",
  dataType: "xml",
  async:false,
  url: 'proxy_new.php',
  data:'country='+$('#country option:selected').attr('value')+'&period=last7d',
  success: function(data, textStatus, jqXHR) {
	
   	xmlresult=data;
	pendingResult=false;
	applyUpdateTown();
  },
  complete: function (jqXHR, textStatus) {
	
  },
  error: function (jqXHR, textStatus, errorThrown) {
	 
  }
  });
}


function updateLanguages() {
	var xml=xmlbanner;
	var found=0;
	langs=[];
	
	$(xml).find($('#country option:selected').attr('value')).each(function (){
			found=1;
			$(this).find('langDefinition').each(function () {
				var title = $(this).text();
				langs.push(title);
			});
	});
	if (found==0)
	{
		$(xml).find('default').each(function (){
			$(this).find('langDefinition').each(function () {
				var title = $(this).text();
				langs.push(title);
			});
		});
	}
	
	var langsList='';
	for(i=0;i<langs.length;i++)
	{

		langsList=langsList+'<option value="'+langs[i]+'"';
		if (defaultLang==langs[i]) langsList+=' selected="selected" ';
		langsList=langsList+'>'+langs[i]+'</option>';
	}
	$('#lang').html(langsList);
	
	initSelect('lang');
}

function updateTechnos() {
	
	var content="";
	var c=$('#country option:selected').attr('value');
	
	/*if ('<?php echo $_GET['country']?>'!='')
	{
		c='<?php echo $_GET['country']?>';
	}*/
	
	
	if (c=='slovenia')
	{

		//phth
		content=content + '<option value="banners-phth"><?php echo $xml->{$langUsed}->phth;?></option>';
		//ph
		content=content + '<option value="banners-ph"><?php echo $xml->{$langUsed}->ph;?></option>';
		//th
		content=content + '<option value="banners-th"><?php echo $xml->{$langUsed}->th;?></option>';
				
	}
	else
	{ 
		if(c=='france')
		{  //all
			content=content + '<option value="banners"><?php echo $xml->{$langUsed}->all;?></option>';
			//phth
			content=content + '<option value="banners-phth"><?php echo $xml->{$langUsed}->phth;?></option>';
			//ph
			content=content + '<option value="banners-ph"><?php echo $xml->{$langUsed}->ph;?></option>';
			//eo
			content=content + '<option value="banners-eo"><?php echo $xml->{$langUsed}->eo;?></option>';
			//th
			content=content + '<option value="banners-th"><?php echo $xml->{$langUsed}->th;?></option>';
			}
		else{
			//all
			content=content + '<option value="banners"><?php echo $xml->{$langUsed}->all;?></option>';
			//theo
			content=content + '<option value="banners-theo"><?php echo $xml->{$langUsed}->theo;?></option>';
			//phth
			content=content + '<option value="banners-phth"><?php echo $xml->{$langUsed}->phth;?></option>';
			//pheo
			content=content + '<option value="banners-pheo"><?php echo $xml->{$langUsed}->pheo;?></option>';
			//ph
			content=content + '<option value="banners-ph"><?php echo $xml->{$langUsed}->ph;?></option>';
			//eo
			content=content + '<option value="banners-eo"><?php echo $xml->{$langUsed}->eo;?></option>';
			//th
			content=content + '<option value="banners-th"><?php echo $xml->{$langUsed}->th;?></option>';
			}
	}
	$('#technos').html(content);
	initSelect('technos');
}

function updateBanner() {
  
  var bannerFile=$('#technos option:selected').attr('value');
  //alert(bannerFile);
  if ((bannerFile==undefined)||(bannerFile==null)||(bannerFile=='')) bannerFile='banners';
  pendingResult=true;
  
  $.ajax({
  type: "GET",
  dataType: "xml",
  async:false,
  url: bannerFile+'.xml',
  data:'',
  success: function(data, textStatus, jqXHR) {
	
   	xmlbanner=data;
	pendingResult=false;
	applyUpdateBanner();
  },
  complete: function (jqXHR, textStatus) {
	
  },
  error: function (jqXHR, textStatus, errorThrown) {
	 
  }
  });
}

function applyUpdateBanner() {
	
	if (pendingResult==false)
	{
		banners=[];
		bannersValue=[];
		var xml=xmlbanner;
		var found=0;
		
		//recherche des bannieres du pays choisi
		$(xml).find($('#country option:selected').attr('value')).each(function (){
			found=1;
			$(this).find('name').each(function () {
				
				var title = $(this).text();
				var name= $(this).parent()[0].nodeName;
				if (!banners.inArray(title))
				{
					banners.push(title);
					bannersValue.push(name);
				}
			});
		
		});
		
		//recherche des bannieres dans pays par défaut.
		if (found==0)
		{
			$(xml).find('default').each(function (){
				$(this).find('name').each(function () {
					var title = $(this).text();
					var name= $(this).parent()[0].nodeName;
					
					if (!banners.inArray(title))
					{
						banners.push(title);
						bannersValue.push(name);
					}
				});
			});
		}
		var bannersList='';
		for(i=0;i<banners.length;i++)
		{
			bannersList=bannersList+'<option value="'+bannersValue[i]+'"';
			if (defaultBanner==bannersValue[i]) bannersList+=' selected="selected" ';
			bannersList=bannersList+'>'+banners[i]+'</option>';
		}
		$('#banner').html(bannersList);
		initSelect('banner');
	}
}

function applyUpdateTown() {
	if (pendingResult==false)
	{
		towns=[];
		townsFill=[];
		var xml=xmlresult;
		var lang = "<?php echo $langUsedTowns; ?>";
		var nameTrad = 'name_'+lang;
		$(xml).find('solar_point').each( function(){ 
			var title = $(this).find(nameTrad).text();
			var titleCode = $(this).find('name').text();
			if (!townsFill.inArray(titleCode))
			{
				towns.push([titleCode,title]);
				townsFill.push(titleCode);
			}
		} );
		towns.sort();
		var townsList='';
		//checkbox version
		for(i=0;i<towns.length;i++)
		{
			var tempTowns=defaultTown.split(',');
			var cbox="box";
			if (tempTowns.inArray(towns[i][0]))
				cbox="boxon";
			townsList=townsList+'<div class="'+cbox+'" myid="'+i+'" id="box'+i+'">&nbsp;</div><input type="checkbox" name="town[]" class="boxhidder" id="town'+i+'" value="'+towns[i][0]+'" ';
			
			if (tempTowns.inArray(towns[i][0]))
			{
				townsList=townsList+' checked="checked" ';
			}
			townsList=townsList+' />&nbsp;<span>'+towns[i][1]+'</span><br clear="all"/>';
		}
		//update values
		$('#town').html(townsList);
		$('#town .box,#town .boxon').each(function(i) {
			$(this).click(function() {
				var id=$(this).attr('myid');
				$('#town'+id).attr('checked');
				if ($('#town'+id).attr('checked')!='checked') {
					$('#town'+id).attr('checked','checked');
					$(this).removeClass('box').addClass('boxon');
				}
				else
				{
					$('#town'+id).removeAttr('checked');
					$(this).removeClass('boxon').addClass('box');
				}
			});
		});
	}
}

function scroller() {
	window.location.href='#cible';
}

function showPreview() {
	checkedTowns='';
	$('#town input:checked').each(function(i) {
		if (checkedTowns!='')
			checkedTowns+=',';
			checkedTowns+=$(this).attr('value');
	});
	$('#preview').css({display:'block'});
	window.setTimeout('scroller()',1000);
	
	<?php
	// multiple image generation
	if (isset ( $_GET ['town'] ) and is_array ( $_GET ['town'] )) {
		$i = 1;
		foreach ( $_GET ['town'] as $town ) {
			?>	
			$('#imagePreview-<?php echo $i; ?>').attr('src','build_trad.php?lang='+$('#lang').val()+'&country='+$('#country').val()+'&period='+$('#period').val()+'&town=<?php echo $town; ?>&format='+$('#banner').val()+'&technos='+$('#technos').val()+'&r='+Math.round(Math.random()*10000));
		<?php
			$i ++;
		}
	} else {
		?>
		$('#imagePreview').attr('src','build_trad.php?lang='+$('#lang').val()+'&country='+$('#country').val()+'&period='+$('#period').val()+'&town='+checkedTowns+'&format='+$('#banner').val()+'&technos='+$('#technos').val()+'&r='+Math.round(Math.random()*10000));
	<?php } ?>
}
</script>
<script src="gblCustomFields.js"></script>
</head>

<body>
	<br />
	<div id="main"
		style="width: 800px; text-align: center; margin-left: auto; margin-right: auto; background-image: url(banners-content/background2.png); background-repeat: repeat-y; background-position: top left;">
		<img src="<?php echo $xml->{$langUsed}->banner;?>" />
		<form action="?" method="get" name="generator" id="generator" style="padding-top: 25px;">
			<table border="0" cellpadding="15" cellspacing="0" width="800" style="text-align: left;">
				<tr style="background-color: #8cbae0;">
					<td><?php echo $xml->{$langUsed}->interface_lang;?></td>
					<td style="width: 238px;"><div id="interfaceLanguageAlt" class="select"></div> <select name="interfaceLanguage" id="interfaceLanguage">
							<option value="en" <?php if (!isset($_GET['languages'])) { echo 'selected="selected"'; } ?>
								<?php if ($_GET['languages']=='en') { echo 'selected="selected"'; } ?>><?php echo $xml->{$langUsed}->lang_en;?></option>
							<option value="fr" <?php if ($_GET['languages']=='fr') { echo 'selected="selected"'; } ?>><?php echo $xml->{$langUsed}->lang_fr;?></option>
							<option value="nl" <?php if ($_GET['languages']=='nl') { echo 'selected="selected"'; } ?>><?php echo $xml->{$langUsed}->lang_nl;?></option>
							<option value="it" <?php if ($_GET['languages']=='it') { echo 'selected="selected"'; } ?>><?php echo $xml->{$langUsed}->lang_it;?></option>
							<option value="pt" <?php if ($_GET['languages']=='pt') { echo 'selected="selected"'; } ?>><?php echo $xml->{$langUsed}->lang_pt;?></option>
							<option value="sl" <?php if ($_GET['languages']=='sl') { echo 'selected="selected"'; } ?>><?php echo $xml->{$langUsed}->lang_sl;?></option>
							<option value="de" <?php if ($_GET['languages']=='de') { echo 'selected="selected"'; } ?>><?php echo $xml->{$langUsed}->lang_de;?></option>
							<option value="hu" <?php if ($_GET['languages']=='hu') { echo 'selected="selected"'; } ?>><?php echo $xml->{$langUsed}->lang_hu;?></option>
							<option value="es" <?php if ($_GET['languages']=='es') { echo 'selected="selected"'; } ?>><?php echo $xml->{$langUsed}->lang_es;?></option>
							<option value="ca" <?php if ($_GET['languages']=='ca') { echo 'selected="selected"'; } ?>><?php echo $xml->{$langUsed}->lang_ca;?></option>
							<option value="sv" <?php if ($_GET['languages']=='sv') { echo 'selected="selected"'; } ?>><?php echo $xml->{$langUsed}->lang_sv;?></option>
					</select></td>
				</tr>
				<tr>
					<td><?php echo $xml->{$langUsed}->countryfield;?></td>
					<td style="width: 238px;"><div id="countryAlt" class="select"></div> <select name="country" id="country"
						onchange="javascript: updateTown(); updateTechnos(); updateBanner(); updateLanguages();">
							<option value="belgium" <?php if (!isset($_GET['country'])) { echo 'selected="selected"'; } ?>
								<?php if ($_GET['country']=='belgium') { echo 'selected="selected"'; } ?>><?php echo $xml->{$langUsed}->belgium;?></option>
							<option value="france" <?php if ($_GET['country']=='france') { echo 'selected="selected"'; } ?>><?php echo $xml->{$langUsed}->france;?></option>
							<option value="italy" <?php if ($_GET['country']=='italy') { echo 'selected="selected"'; } ?>><?php echo $xml->{$langUsed}->italy;?></option>
							<option value="portugal" <?php if ($_GET['country']=='portugal') { echo 'selected="selected"'; } ?>><?php echo $xml->{$langUsed}->portugal;?></option>
							<option value="slovenia" <?php if ($_GET['country']=='slovenia') { echo 'selected="selected"'; } ?>><?php echo $xml->{$langUsed}->slovenia;?></option>
							<option value="germany" <?php if ($_GET['country']=='germany') { echo 'selected="selected"'; } ?>><?php echo $xml->{$langUsed}->germany;?></option>
							<option value="hungary" <?php if ($_GET['country']=='hungary') { echo 'selected="selected"'; } ?>><?php echo $xml->{$langUsed}->hungary;?></option>
							<option value="spain" <?php if ($_GET['country']=='spain') { echo 'selected="selected"'; } ?>><?php echo $xml->{$langUsed}->spain;?></option>
							<option value="sweden" <?php if ($_GET['country']=='sweden') { echo 'selected="selected"'; } ?>><?php echo $xml->{$langUsed}->sweden;?></option>
							<option value="united_kingdom" <?php if ($_GET['country']=='united_kingdom') { echo 'selected="selected"'; } ?>><?php echo $xml->{$langUsed}->united_kingdom;?></option>
					</select></td>
					<td rowspan="5" valign="top" style="width: 340px; border-left: 3px dashed #8cbae0">
						<?php echo $xml->{$langUsed}->codetitle;?><br /> <br /> <textarea id="embed" class="textCode"></textarea> <span
						style="font-size: 11px; width: 298px; margin-top: 8px; display: block; text-align: justify;"><?php echo $xml->{$langUsed}->codetext;?></span> <input type="submit" name="submit" id="submit" class="copyCode" value="<?php echo $xml->{$langUsed}->embedtext;?>" style="margin-top:25px; text-indent:-10000px; border:0; background:none; background-image:url(<?php echo $xml->{$langUsed}->embedbackground;?>); width:188px; height:53px; cursor:pointer; " /><input
						type="hidden" name="languages" value="<?php echo $langUsed?>" />
					</td>
				</tr>

				<tr>
					<td><?php echo $xml->{$langUsed}->periodfield;?></td>
					<td><div id="periodAlt" class="select"></div> <select name="period" id="period" onchange="updateTown();">
							<option value="daily" <?php if ($_GET['period']=='daily') { echo 'selected="selected"'; } ?>><?php echo $xml->{$langUsed}->lastday;?></option>
							<option value="weekly" <?php if (!isset($_GET['period'])) { echo 'selected="selected"'; } ?>
								<?php if ($_GET['period']=='weekly') { echo 'selected="selected"'; } ?>><?php echo $xml->{$langUsed}->lastsevenday;?></option>
					</select></td>
				</tr>
				<tr>
					<td valign="top"><?php echo $xml->{$langUsed}->townfield;?><br /> <span style="font-size: 11px; margin-top: 8px; display: block;"><?php echo $xml->{$langUsed}->towntext;?></span></td>
					<td><div id="town"></div></td>
				</tr>
				<tr>
					<td colspan="2" align="left">
						<?php echo $xml->{$langUsed}->embedtechnos;?><br /> <br />
						<div id="technosAlt" class="select" style="width: 380px; background-image: url(gblCustomSelectLong.png);"></div> <select name="technos"
						id="technos" onchange="javascript: updateBanner();">
							<?php if ($country!="slovenia") { ?><option value="banners"><?php echo $xml->{$langUsed}->all;?></option><?php } ?>
							<?php if ($country!="slovenia") { ?><option value="banners-theo"><?php echo $xml->{$langUsed}->theo;?></option><?php } ?>
							<option value="banners-phth"><?php echo $xml->{$langUsed}->phth;?></option>
							<?php if ($country!="slovenia") { ?><option value="banners-pheo"><?php echo $xml->{$langUsed}->pheo;?></option><?php } ?>
							<option value="banners-ph"><?php echo $xml->{$langUsed}->ph;?></option>
							<?php if ($country!="slovenia") { ?><option value="banners-eo"><?php echo $xml->{$langUsed}->eo;?></option><?php } ?>
							<option value="banners-th"><?php echo $xml->{$langUsed}->th;?></option>
					</select>
					</td>
				</tr>
				<tr>
					<td><?php echo $xml->{$langUsed}->bannerfield;?></td>
					<td><div id="bannerAlt" class="select"></div> <select name="banner" id="banner"></select></td>
				</tr>
				<tr>
					<td valign="top" style="padding-top: 15px;"><?php echo $xml->{$langUsed}->languagefield;?></td>
					<td valign="top" style="padding-top: 15px;"><div id="langAlt" class="select"></div> <select name="lang" id="lang"></select> <input type="button" name="prev" id="prev" value="<?php echo $xml->{$langUsed}->showpreviewtext;?>" style="text-indent:-10000px; margin-top:60px; border:0; background:none; background-image:url(<?php echo $xml->{$langUsed}->showpreviewbackground;?>); width:200px; height:54px; cursor:pointer;" />
					</td>
				</tr>
				<tr>
					<td colspan="3" align="center">&nbsp;&nbsp;&nbsp;</td>
				</tr>
			</table>
		</form>
		<h3 style="display: none;"><?php echo $xml->{$langUsed}->previewfield;?></h3>
		<br />

		<div id="preview" style="display: none;">
			<?php
			// multiple image generation
			if (isset ( $_GET ['town'] ) and is_array ( $_GET ['town'] )) {
				$i = 1;
				foreach ( $_GET ['town'] as $town ) {
					?>	
					<img id="imagePreview-<?php echo $i; ?>" src="" alt="" />	
				<?php
					$i ++;
				}
			} else {
				?>
				<img id="imagePreview" src="" alt="" />
			<?php } ?>
		</div>

		<a href="javascript:void(0);" name="cible" id="cible" style="text-decoration: none;">&nbsp;</a><br />
		<script>
			$(document).ready(function(){			
				// select interface language
				var urlLoad; 
				$('#interfaceLanguage').change(function(){
					setGetParameter('languages', $(this).val())
				});
				function setGetParameter(paramName, paramValue)
				{
				    var url = window.location.href;
				    if (url.indexOf(paramName + "=") >= 0)
				    {
				        var prefix = url.substring(0, url.indexOf(paramName));
				        var suffix = url.substring(url.indexOf(paramName));
				        suffix = suffix.substring(suffix.indexOf("=") + 1);
				        suffix = (suffix.indexOf("&") >= 0) ? suffix.substring(suffix.indexOf("&")) : "";
				        url = prefix + paramName + "=" + paramValue + suffix;
				    }
				    else
				    {
				    if (url.indexOf("?") < 0)
				        url += "?" + paramName + "=" + paramValue;
				    else
				        url += "&" + paramName + "=" + paramValue;
				    }
				    document.location.href = url; 
				}
			
				updateTechnos();
				$('#technos option[value="'+defaultTechnos+'"]').attr('selected','selected');
				
				updateTown();
				updateBanner();
				updateLanguages();
				
				$('#prev').click(function () {
					//showPreview();	
					$('#submit').click();
					
				});
				
				<?php if ($_GET ['submit'] == $xml->{$langUsed}->embedtext) { ?>
					showPreview();
					var xml=xmlbanner;
					var found=0;
					var linked ='';
					$(xml).find($('#country').val()).each(function (){
						found=1;
						$(this).find('urlmain').each(function () {
							linked=$(this).text();
						});
					});
					if (found==0)
					{
						$(xml).find('default').each(function (){
							$(this).find('urlmain').each(function () {
								linked=$(this).text();
							});
						});
					}	
					
					var formatwidth = "135px";
					var formatheight = "255px";
					if ($('#banner').val() == 'b120600') {
						formatwidth = "135px";
						formatheight = "616px";
					} else if ($('#banner').val() == 'b468060') {
						formatwidth = "484px";
						formatheight = "77px";
					} else if ($('#banner').val() == 'b160600') {
						formatwidth = "175px";
						formatheight = "616px";
					} else if ($('#banner').val() == 'b728090') {
						formatwidth = "745px";
						formatheight = "110px";
					}
				
					<?php
					if (isset ( $_GET ['town'] ))
						$townembed = implode ( ',', $_GET ['town'] );
					$townembed = urlencode ( $townembed );
					?>
				
					var embedContent = '&lt;iframe frameborder="0" height="'+formatheight+'" width="'+formatwidth+'" src="http://www.energizair.eu/embed/energizair_embed.php?townArray=<?php echo $townembed; ?>&lang='+$('#lang').val()+'&country='+$('#country').val()+'&period='+$('#period').val()+'&format='+$('#banner').val()+'&technos='+$('#technos').val()+'&r='+Math.round(Math.random()*10000)+'"></iframe>';
					$('#embed').html(embedContent);
					$('#embed').css({display:'block'});
					$('#titlecode').css({display:'block'});
			
				<?php }	?>
				
				initSelect('country');
				initSelect('period');
				initSelect('technos');
				initSelect('banner');
				initSelect('lang');
				initSelect('interfaceLanguage');
			
			});
			
			jQuery(window).load(function(){ 
				// carrousel
				$("#preview").carouFredSel({
					items : {
						start       	: "random",
						visible 		: 1
					},			
					direction			: "left",
					scroll : {
						items			: 1,
						easing			: "swing",
						duration		: 1000,							
						pauseOnHover	: true
					}					
				});	
				$(".caroufredsel_wrapper").css("margin","0 auto");
			});
		
		</script>
	</div>
</body>
</html>