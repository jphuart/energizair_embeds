<?php

/**
 * 
 * @author jph
 *
 *
 * http://localhost/energizair_eu/embed/build_trad.php?lang=de&country=germany&period=weekly&town=aachen&format=b120240&technos=banners&r=12345
 */
class BannerImage {
	public $country; // country
	public $period;
	public $town;
	public $technos;
	public $banner;
	public $lang;

	public function __construct($arr_param) {
		$this->country = (isset($arr_param['country']) ? strtolower($arr_param['country']) : 'belgium');
		$this->period = (isset($arr_param['period']) ? $arr_param['period'] : 'weekly');
		$this->town = (isset($arr_param['town']) ? $arr_param['town'] : 'brussels');
		$this->technos = (isset($arr_param['technos']) ? $arr_param['technos'] : 'banners');
		$this->banner = (isset($arr_param['format']) ? $arr_param['format'] : 'line');
		$this->lang = (isset($arr_param['lang']) ? $arr_param['lang'] : 'en');
		/**
		 * set the locale to be used for proper display of dates
		 */
		$this->_set_locale();
		/**
		 * load the reference data from xml files
		 */
		$this->_load_data();
		return;
	}

	public function __destruct() {
		unset($this);
	}

	private function _add_element_pv() {
		/**
		 * Add PV
		 */
		$imphotovoltaique = imagecreatefrompng('banners-content/images/' . $this->xml_banner->photovoltaique->image->url);
		list ($photovoltaiquewidth, $photovoltaiqueheight, $photovoltaiquetype, $photovoltaiqueattr) = getimagesize('banners-content/images/' . $this->xml_banner->photovoltaique->image->url);
		imagecopy($this->image, $imphotovoltaique, strval($this->xml_banner->photovoltaique->image->position->left), strval($this->xml_banner->photovoltaique->image->position->top), 0, 0, $photovoltaiquewidth, $photovoltaiqueheight);
		/**
		 * PV text
		 */
		foreach ($this->xml_banner->photovoltaique->content as $textual) {
			$colors = $this->_rgb2array($textual->lang->color);
			$color = imagecolorallocate($this->image, $colors[0], $colors[1], $colors[2]);
			$trads = $textual;
			$text = "";
			foreach ($trads as $t) {
				if ((string) $t->attributes()->id == $this->lang) {
					$text = $t->value;
				}
			}
			$font = (string) $textual->lang->font . '.ttf';
			
			$decal = 0;
			$bbox = imageftbbox((int) $textual->lang->size * 0.7, 0, $font, $text);
			$maxWidth = (int) $textual->lang->width;
			$currentWidth = $bbox[4] - $bbox[6];
			$decal = round(($maxWidth - $currentWidth) / 2, 0);
			if ($textual->lang->center != 'true') {
				$decal = 0;
			}
			imagettftext($this->image, (int) $textual->lang->size * 0.7, 0, $decal + (int) $textual->lang->position->left, (int) $textual->lang->position->top + (int) $textual->lang->size * 0.7, $color, $font, $text);
		}
		/**
		 * PV data
		 */
		$dataValue = null;
		$dataValueList = $this->xml_data->PhotoVoltEnergy->solar_point;
		$dataValue = $this->_getTownNode($dataValueList);
		$textual = $this->xml_banner->photovoltaique->data;
		$colors = $this->_rgb2array($textual->lang->color);
		$color = imagecolorallocate($this->image, $colors[0], $colors[1], $colors[2]);
		$text = $dataValue->NeedscoveragePV . "%";
		$font = (string) $textual->lang->font . '.ttf';
		
		$decal = 0;
		$bbox = imageftbbox((int) $textual->lang->size * 0.7, 0, $font, $text);
		$maxWidth = (int) $textual->lang->width;
		$currentWidth = $bbox[4] - $bbox[6];
		$decal = round(($maxWidth - $currentWidth) / 2, 0);
		if ($textual->lang->center != 'true') {
			$decal = 0;
		}
		imagettftext($this->image, (int) $textual->lang->size * 0.7, 0, $decal + (int) $textual->lang->position->left, (int) $textual->lang->position->top + (int) $textual->lang->size * 0.7, $color, $font, $text);
		return;
	}

	private function _add_element_soltherm() {
		/**
		 * Add solar thermal
		 */
		$imthermique = imagecreatefrompng('banners-content/images/' . $this->xml_banner->thermique->image->url);
		list ($thermiquewidth, $thermiqueheight, $thermiquetype, $thermiqueattr) = getimagesize('banners-content/images/' . $this->xml_banner->thermique->image->url);
		imagecopy($this->image, $imthermique, strval($this->xml_banner->thermique->image->position->left), strval($this->xml_banner->thermique->image->position->top), 0, 0, $thermiquewidth, $thermiqueheight);
		/**
		 * Solar Therm text
		 */
		foreach ($this->xml_banner->thermique->content as $textual) {
			$colors = $this->_rgb2array($textual->lang->color);
			$color = imagecolorallocate($this->image, $colors[0], $colors[1], $colors[2]);
			$trads = $textual;
			$text = "";
			foreach ($trads as $t) {
				if ((string) $t->attributes()->id == $this->lang) {
					$text = $t->value;
				}
			}
			$font = (string) $textual->lang->font . '.ttf';
			$decal = 0;
			$bbox = imageftbbox((int) $textual->lang->size * 0.7, 0, $font, $text);
			$maxWidth = (int) $textual->lang->width;
			$currentWidth = $bbox[4] - $bbox[6];
			$decal = round(($maxWidth - $currentWidth) / 2, 0);
			if ($textual->lang->center != 'true') {
				$decal = 0;
			}
			imagettftext($this->image, (int) $textual->lang->size * 0.7, 0, $decal + (int) $textual->lang->position->left, (int) $textual->lang->position->top + (int) $textual->lang->size * 0.7, $color, $font, $text);
		}
		/**
		 * Solar Therm data
		 */
		$dataValue = null;
		$dataValueList = $this->xml_data->SolarTherm->solar_point;
		$dataValue = $this->_getTownNode($dataValueList);
		
		$textual = $this->xml_banner->thermique->data;
		$colors = $this->_rgb2array($textual->lang->color);
		$color = imagecolorallocate($this->image, $colors[0], $colors[1], $colors[2]);
		$text = $dataValue->NeedscoverageTH . "%";
		$font = (string) $textual->lang->font . '.ttf';
		
		$decal = 0;
		$bbox = imageftbbox((int) $textual->lang->size * 0.7, 0, $font, $text);
		$maxWidth = (int) $textual->lang->width;
		$currentWidth = $bbox[4] - $bbox[6];
		$decal = round(($maxWidth - $currentWidth) / 2, 0);
		if ($textual->lang->center != 'true') {
			$decal = 0;
		}
		imagettftext($this->image, (int) $textual->lang->size * 0.7, 0, $decal + (int) $textual->lang->position->left, (int) $textual->lang->position->top + (int) $textual->lang->size * 0.7, $color, $font, $text);
		return;
	}

	private function _add_element_wind() {
		/**
		 * Add Wind
		 */
		$imeolien = imagecreatefrompng('banners-content/images/' . $this->xml_banner->eolien->image->url);
		list ($eolienwidth, $eolienheight, $eolientype, $eolienattr) = getimagesize('banners-content/images/' . $this->xml_banner->eolien->image->url);
		imagecopy($this->image, $imeolien, strval($this->xml_banner->eolien->image->position->left), strval($this->xml_banner->eolien->image->position->top), 0, 0, $eolienwidth, $eolienheight);
		/**
		 * Wind text
		 */
		// $cptx = 0;
		foreach ($this->xml_banner->eolien->content as $textual) {
			$colors = $this->_rgb2array($textual->lang->color);
			$color = imagecolorallocate($this->image, $colors[0], $colors[1], $colors[2]);
			$trads = $textual;
			$text = "";
			
			foreach ($trads as $t) {
				// $cptx ++;
				if ((string) $t->attributes()->id == $this->lang) {
					$text = $t->value;
				}
				
				// $dataValue = null;
				// $dataValueList = $this->xml_data->PhotoVoltEnergy->solar_point;
				// $dataValue = $this->_getTownNode($dataValueList);
				// if (($cptx == 1) && ($text != '') && ($this->banner != "b125125") && ($dataValue->display_nat_ref)) {
				// if ($this->country == 'belgium') {
				// $text .= " (BE)";
				// }
				// if ($this->country == 'italy') {
				// $text .= " (IT)";
				// }
				// if ($this->country == 'slovenia') {
				// $text .= " (SL)";
				// }
				// if ($this->country == 'portugal') {
				// $text .= " (PT)";
				// }
				// if ($this->country == 'france') {
				// $text .= " (FR)";
				// }
				// }
			}
			$font = (string) $textual->lang->font . '.ttf';
			
			$decal = 0;
			$bbox = imageftbbox((int) $textual->lang->size * 0.7, 0, $font, $text);
			$maxWidth = (int) $textual->lang->width;
			$currentWidth = $bbox[4] - $bbox[6];
			$decal = round(($maxWidth - $currentWidth) / 2, 0);
			if ($textual->lang->center != 'true') {
				$decal = 0;
			}
			imagettftext($this->image, (int) $textual->lang->size * 0.7, 0, $decal + (int) $textual->lang->position->left, (int) $textual->lang->position->top + (int) $textual->lang->size * 0.7, $color, $font, $text);
		}
		/**
		 * wind data
		 */
		$dataValue = null;
		$dataValueList = $this->xml_data->PhotoVoltEnergy->solar_point;
		$dataValue = $this->_getTownNode($dataValueList);
		$area = $dataValue->parentwindarea;
		
		$dataValue = null;
		$dataValueList = $this->xml_data->WindEnergy->Areas;
		
		foreach ($dataValueList->Area as $t) {
			if (strtolower((string) $t->name) == strtolower($area)) {
				$dataValue = $t;
				break;
			}
		}
		
		$textual = $this->xml_banner->eolien->data;
		$colors = $this->_rgb2array($textual->lang->color);
		$color = imagecolorallocate($this->image, $colors[0], $colors[1], $colors[2]);
		
		$text = number_format($dataValue->NumberOfHouses, 0, ",", ".");
		
		$font = (string) $textual->lang->font . '.ttf';
		$decal = 0;
		$bbox = imageftbbox((int) $textual->lang->size * 0.7, 0, $font, $text);
		$maxWidth = (int) $textual->lang->width;
		$currentWidth = $bbox[4] - $bbox[6];
		$decal = round(($maxWidth - $currentWidth) / 2, 0);
		if ($textual->lang->center != 'true') {
			$decal = 0;
		}
		imagettftext($this->image, (int) $textual->lang->size * 0.7, 0, $decal + (int) $textual->lang->position->left, (int) $textual->lang->position->top + (int) $textual->lang->size * 0.7, $color, $font, $text);
		return;
	}

	private function _getTownNode($towns_list) {
		foreach ($towns_list as $t) {
			if (((string) $t->name == $this->town) or ((string) $t->name_en == $this->town)) {
				$node = $t;
			}
		}
		return $node;
	}

	private function _getTownTranslatedName($towns_list) {
		$nameLang = "name_" . $this->lang;
		$node = $this->_getTownNode($towns_list);
		$townValue = $node->$nameLang;
		
		return $townValue;
	}

	private function _load_data() {
		$xmlBan = file_get_contents($this->technos . '.xml');
		$file = 'tmp/' . $this->country . '_' . $this->period . '.xml';
		$xmlData = file_get_contents($file);
		// $xmlData = file_get_contents ( 'http://ws.energizair.eu/wspartners/default?country=' . $this->country . '&period=' . $this->period . '&r=' . rand ( 1, 100000 ) );
		
		$xml_banner = @simplexml_load_string($xmlBan);
		$this->xml_data = @simplexml_load_string($xmlData);
		
		if (isset($xml_banner->{$this->country})) {
			$this->xml_banner = $xml_banner->{$this->country}->{$this->banner};
		}
		else {
			$this->xml_banner = $xml_banner->default->{$this->banner};
		}
		
		return;
	}

	private function _reduceTownName($chaine, $nb_car, $delim = '...') {
		$length = $nb_car;
		if ($nb_car < strlen($chaine)) {
			while (($chaine{$length} != " ") && ($length > 0)) {
				$length --;
			}
			if ($length == 0) {
				return substr($chaine, 0, $nb_car) . $delim;
			}
			else {
				return substr($chaine, 0, $length) . $delim;
			}
		}
		else {
			return $chaine;
		}
	}

	private function _rgb2array($rgb) {
		return array(
				base_convert(substr($rgb, 0, 2), 16, 10),
				base_convert(substr($rgb, 2, 2), 16, 10),
				base_convert(substr($rgb, 4, 2), 16, 10) 
		);
	}

	private function _set_date_to_display($fromDateDisplay, $toDateDisplay) {
		$date_params = array();
		$date_params['dateDisplay2'] = '';
		$date_params['dateDisplay2b'] = '';
		$date_params['dateDisplay2c'] = '';
		$date_params['addon'] = 0;
		
		$yearFrom = substr($fromDateDisplay, 0, 4);
		$monthFrom = substr($fromDateDisplay, 5, 2);
		$dayFrom = substr($fromDateDisplay, 8, 2);
		
		$yearTo = substr($toDateDisplay, 0, 4);
		$monthTo = substr($toDateDisplay, 5, 2);
		$dayTo = substr($toDateDisplay, 8, 2);
		
		$decalDate = date("N", strtotime($toDateDisplay));
		$dateDisplay0 = date("Y-m-d", mktime("0", "0", "0", date("m", strtotime($toDateDisplay)), date("d", strtotime($toDateDisplay)) - 6, date("Y", strtotime($toDateDisplay))));
		
		$yearDisplay = substr($dateDisplay0, 0, 4);
		$monthDisplay = substr($dateDisplay0, 5, 2);
		$dayDisplay = substr($dateDisplay0, 8, 2);
		
		$startDay = $dayDisplay;
		$startDayChiffre = strtolower(strftime("%d", mktime(0, 0, 0, 1, $dayDisplay, 1)));
		$startMonth = strtolower(strftime("%b", mktime(0, 0, 0, $monthDisplay, 1, 1)));
		$startMonthN = strtolower(strftime("%m", mktime(0, 0, 0, $monthDisplay, 1, 1)));
		$startMonthLong = strtolower(strftime("%B", mktime(0, 0, 0, $monthDisplay, 1, 1)));
		$startYear = $yearDisplay;
		$endDay = date("d", mktime(0, 0, 0, $monthDisplay, $startDay + 6, $startYear));
		$endMonth = strtolower(strftime("%b", mktime(0, 0, 0, $monthDisplay, $startDay + 6, $startYear)));
		$endMonthN = strtolower(strftime("%m", mktime(0, 0, 0, $monthDisplay, $startDay + 6, $startYear)));
		$endMonthLong = strtolower(strftime("%B", mktime(0, 0, 0, $monthDisplay, $startDay + 6, $startYear)));
		
		if ($this->period == "weekly") {
			// date week text
			$dateWeekText = $this->xml_banner->date->content->lang->value;
			foreach ($this->xml_banner->date->content as $textual) {
				$trads = $textual;
				foreach ($trads as $t) {
					if ((string) $t->attributes()->id == $this->lang) {
						$dateWeekText = $t->value;
					}
				}
			}
			
			if ($this->xml_banner->date->format == 'line') {
				$date_params['dateDisplay2'] = $dateWeekText . " du " . $startDay . " " . $startMonth . " au " . $endDay . " " . $endMonth;
				if ($this->lang == 'fr') {
					$date_params['dateDisplay2'] = $dateWeekText . " du " . $startDay . " " . $startMonth . " au " . $endDay . " " . $endMonth;
				}
				if ($this->lang == 'it') {
					$date_params['dateDisplay2'] = $dateWeekText . " dal " . $startDay . " " . $startMonth . " al " . $endDay . " " . $endMonth;
				}
				if ($this->lang == 'sl') {
					switch ($this->banner) {
						case 'b120240' :
						case 'b655090' :
							if (isset($this->xml_banner->date->formatLong) && ($this->xml_banner->date->formatLong == "true")) {
								$date_params['dateDisplay2'] = $dateWeekText . " od " . $startDayChiffre . ". " . $startMonth . " do " . $endDay . ". " . $endMonth;
							}
							else {
								$date_params['dateDisplay2'] = $dateWeekText . $startDayChiffre . ". " . $startMonth . " do " . $endDay . ". " . $endMonth;
							}
							break;
						
						case 'b468060' :
						case 'b728090' :
							if (isset($this->xml_banner->date->formatLong) && ($this->xml_banner->date->formatLong == "true")) {
								$date_params['dateDisplay2'] = $dateWeekText . " od " . $startDayChiffre . ". " . $startMonthLong . " do " . $endDay . ". " . $endMonthLong;
							}
							else {
								$date_params['dateDisplay2'] = $dateWeekText . $startDayChiffre . ". " . $startMonthLong . " do " . $endDay . ". " . $endMonthLong;
							}
							break;
						case 'b160600' :
							if (isset($this->xml_banner->date->formatLong) && ($this->xml_banner->date->formatLong == "true")) {
								$date_params['dateDisplay2'] = $dateWeekText . " od " . $startDayChiffre . ". " . $startMonth . ($startMonth != "maj" ? "." : "") . " do " . $endDay . " " . $endMonth . "";
							}
							else {
								$date_params['dateDisplay2'] = $dateWeekText . $startDayChiffre . ". " . $startMonth . ($startMonth != "maj" ? "." : "") . " do " . $endDay . ". " . $endMonth . ($endMonth != "maj" ? "." : "") . "";
							}
							break;
						
						case 'b125125' :
							if (isset($this->xml_banner->date->formatLong) && ($this->xml_banner->date->formatLong == "true")) {
								$date_params['dateDisplay2'] = $dateWeekText . " od " . $startDayChiffre . ". " . $startMonth . " do " . $endDay . ". " . $endMonth;
							}
							else {
								$date_params['dateDisplay2'] = $dateWeekText . $startDayChiffre . ". " . $startMonth . ($startMonth != "maj" ? "." : "") . " do " . $endDay . ". " . $endMonth . ($endMonth != "maj" ? "." : "") . "";
							}
							break;
					}
				}
				if ($this->lang == 'pt') {
					$date_params['dateDisplay2'] = $dateWeekText . " de " . $startDay . " " . $startMonth . " a " . $endDay . " " . $endMonth;
				}
				if ($this->lang == 'en') {
					$date_params['dateDisplay2'] = $dateWeekText . " " . $startDay . " " . ucfirst($startMonth) . " to " . $endDay . " " . ucfirst($endMonth);
				}
				if ($this->lang == 'es') {
					$date_params['dateDisplay2'] = $dateWeekText . " del " . $startDay . " " . $startMonth . " al " . $endDay . " " . $endMonth;
				}
				if ($this->lang == 'ca') {
					$date_params['dateDisplay2'] = $dateWeekText . " del " . $startDay . " " . $startMonth . " al " . $endDay . " " . $endMonth;
				}
				if ($this->lang == 'de') {
					$date_params['dateDisplay2'] = $dateWeekText . " " . $startDay . ". " . ucfirst($startMonth) . " bis " . $endDay . ". " . ucfirst($endMonth);
				}
				if ($this->lang == 'hu') {
					$date_params['dateDisplay2'] = $dateWeekText . " " . $startMonth . ". " . $startDay . ". - " . $endMonth . ". " . $endDay . ".";
				}
				if ($this->lang == 'sv') {
					$date_params['dateDisplay2'] = $dateWeekText . " " . $startDay . " " . $startMonth . " till " . $endDay . " " . $endMonth;
				}
				if ($this->lang == 'nl') {
					$date_params['dateDisplay2'] = $dateWeekText . " " . $startDay . " " . ucfirst($startMonth) . " tot " . $endDay . " " . ucfirst($endMonth);
				}
			}
			else {
				$date_params['addon'] = 1;
				/**
				 * default is french
				 */
				$date_params['dateDisplay2'] = $startDay . " " . $startMonth;
				$date_params['dateDisplay2b'] = "au";
				$date_params['dateDisplay2c'] = $endDay . " " . $endMonth;
				
				if ($this->lang == 'it') {
					$date_params['dateDisplay2b'] = " al ";
				}
				if ($this->lang == 'sl') {
					$date_params['dateDisplay2'] = $startDay . ". " . $startMonth . ($startMonth != "maj" ? "." : "");
					$date_params['dateDisplay2b'] = " do ";
					$date_params['dateDisplay2c'] = $endDay . ". " . $endMonth . ($endMonth != "maj" ? "." : "");
				}
				if ($this->lang == 'pt') {
					$date_params['dateDisplay2b'] = " a ";
				}
				if ($this->lang == 'en') {
					$date_params['dateDisplay2'] = $startDay . " " . ucfirst($startMonth);
					$date_params['dateDisplay2b'] = " to ";
					$date_params['dateDisplay2c'] = $endDay . " " . ucfirst($endMonth);
				}
				if ($this->lang == 'sv') {
					$date_params['dateDisplay2b'] = " till ";
				}
				if ($this->lang == 'de') {
					$date_params['dateDisplay2'] = $startDay . ". " . ucfirst($startMonth);
					$date_params['dateDisplay2b'] = " bis ";
					$date_params['dateDisplay2c'] = $endDay . ". " . ucfirst($endMonth);
				}
				if ($this->lang == 'es') {
					$date_params['dateDisplay2b'] = " al ";
				}
				if ($this->lang == 'ca') {
					$date_params['dateDisplay2b'] = " al ";
				}
				if ($this->lang == 'hu') {
					$date_params['dateDisplay2'] = $startMonth . ". " . $startDay . ".";
					$date_params['dateDisplay2b'] = " - ";
					$date_params['dateDisplay2c'] = $endMonth . ". " . $endDay . ".";
				}
				if ($this->lang == 'nl') {
					$date_params['dateDisplay2'] = $startDay . ". " . ucfirst($startMonth);
					$date_params['dateDisplay2b'] = " tot ";
					$date_params['dateDisplay2c'] = $endDay . ". " . ucfirst($endMonth);
				}
			}
		}
		elseif ($this->period == "daily") {
			if ($this->xml_banner->date->format == 'line') {
				$val_date_jour_1 = time() + (- 1) * 24 * 3600;
				$val_date_jour_1 = strtotime($fromDateDisplay);
				if (isset($this->xml_banner->date->formatLong) && ($this->xml_banner->date->formatLong == "true")) {
					$date_params['dateDisplay2'] = (strftime("%A %d %B", $val_date_jour_1));
					if ($this->lang == 'sl') {
						$date_params['dateDisplay2'] = (strftime("%A, %d. %B", $val_date_jour_1));
						// if ((($this->banner == "b468060") || ($this->banner == "b728090") || ($this->banner == "b655090")) && ($decalDate == 4)) {
						// $date_params ['dateDisplay2'] = (strftime ( ", %d. %B", $val_date_jour_1 ));
						// }
					}
					if ($this->lang == 'hu') {
						$date_params['dateDisplay2'] = (strftime("%B %d.", $val_date_jour_1));
					}
					if ($this->lang == 'pt') {
						$date_params['dateDisplay2'] = (strftime("%A-feira %d %B", $val_date_jour_1));
					}
				}
				else {
					if (isset($this->xml_banner->date->formatLong) && ($this->xml_banner->date->formatLong == "medium")) {
						$date_params['dateDisplay2'] = (strftime("%d %B", $val_date_jour_1));
						if ($this->lang == 'sl') {
							$date_params['dateDisplay2'] = (strftime("%d. %B", $val_date_jour_1));
						}
						if ($this->lang == 'hu') {
							$date_params['dateDisplay2'] = (strftime("%B %d.", $val_date_jour_1));
						}
					}
					else {
						$date_params['dateDisplay2'] = (strftime("%d %h", $val_date_jour_1));
						if ($this->lang == 'sl') {
							if (strftime("%h", $val_date_jour_1) != "maj") {
								$date_params['dateDisplay2'] = (strftime("%d. %h.", $val_date_jour_1));
							}
							else {
								$date_params['dateDisplay2'] = (strftime("%d. %h", $val_date_jour_1));
							}
						}
						if ($this->lang == 'hu') {
							$date_params['dateDisplay2'] = (strftime("%h %d.", $val_date_jour_1));
						}
					}
				}
			}
			else {
				$val_date_jour_1 = time() + (- 1) * 24 * 3600;
				$val_date_jour_1 = strtotime($fromDateDisplay);
				
				if (isset($this->xml_banner->date->formatLong) && ($this->xml_banner->date->formatLong == "true")) {
					$date_params['dateDisplay2'] = (strftime("%A %d %B", $val_date_jour_1));
					if ($this->lang == 'sl') {
						$date_params['dateDisplay2'] = (strftime("%A, %d. %B", $val_date_jour_1));
					}
					if ($this->lang == 'hu') {
						$date_params['dateDisplay2'] = (strftime("%B %d.", $val_date_jour_1));
					}
					if ($this->lang == 'pt') {
						$date_params['dateDisplay2'] = (strftime("%A-feira %d %B", $val_date_jour_1));
					}
				}
				else {
					$date_params['dateDisplay2'] = (strftime("%d %h", $val_date_jour_1));
					if ($this->lang == 'sl') {
						if (strftime("%h", $val_date_jour_1) != "maj") {
							$date_params['dateDisplay2'] = (strftime("%d. %h.", $val_date_jour_1));
						}
						else {
							$date_params['dateDisplay2'] = (strftime("%d. %h", $val_date_jour_1));
						}
					}
					if ($this->lang == 'hu') {
						$date_params['dateDisplay2'] = (strftime("%h %d.", $val_date_jour_1));
					}
				}
				
				$date_params['dateDisplay2b'] = $date_params['dateDisplay2'];
				$date_params['dateDisplay2'] = "";
				$date_params['dateDisplay2c'] = "";
				$date_params['addon'] = 1;
			}
		}
		return $date_params;
	}

	private function _set_locale() {
		if ($this->lang == 'pt') {
			setlocale(LC_ALL, 'pt_PT');
		}
		if ($this->lang == 'fr') {
			setlocale(LC_ALL, 'fr_FR');
		}
		if ($this->lang == 'en') {
			setlocale(LC_ALL, 'en_EN');
		}
		if ($this->lang == 'it') {
			setlocale(LC_ALL, 'it_IT');
		}
		if ($this->lang == 'sl') {
			setlocale(LC_ALL, 'sl_SI.UTF8');
		}
		if ($this->lang == 'de') {
			setlocale(LC_ALL, 'de_DE');
		}
		if ($this->lang == 'hu') {
			setlocale(LC_ALL, 'hu_HU');
		}
		if ($this->lang == 'es') {
			setlocale(LC_ALL, 'es_ES');
		}
		if ($this->lang == 'ca') {
			setlocale(LC_ALL, 'es_CA');
		}
		if ($this->lang == 'sv') {
			setlocale(LC_ALL, 'sv_SE');
		}
		if ($this->lang == 'nl') {
			setlocale(LC_ALL, 'nl_NL');
		}
		return;
	}

	public function build_banner() {
		$towns = explode(',', $this->town);
		if (count($towns) > 1) {
			$this->town = $towns[rand(0, count($towns) - 1)];
		}
		/**
		 * Get the translation of the name of the town
		 */
		$townTradList = $this->xml_data->PhotoVoltEnergy->solar_point;
		$townValue = $this->_getTownTranslatedName($townTradList);
		/**
		 * Get sizes of the banner
		 */
		$sizes = explode('x', $this->xml_banner->name);
		$width = $sizes[0];
		$height = $sizes[1];
		/**
		 * Get background
		 */
		$background = $this->xml_banner->background->image->url;
		/**
		 * Start building the image
		 */
		$this->image = imagecreatefrompng('banners-content/images/' . $background);
		/**
		 * Add the logo
		 */
		$imlogo = imagecreatefrompng('banners-content/images/' . $this->xml_banner->logo->image->url);
		list ($logowidth, $logoheight, $logotype, $logoattr) = getimagesize('banners-content/images/' . $this->xml_banner->logo->image->url);
		imagecopy($this->image, $imlogo, strval($this->xml_banner->logo->image->position->left), strval($this->xml_banner->logo->image->position->top), 0, 0, $logowidth, $logoheight);
		/**
		 * Add the title image
		 */
		if ($this->xml_banner->title->image->url) {
			$imtitle = imagecreatefrompng('banners-content/images/' . $this->xml_banner->title->image->url);
			list ($titlewidth, $titleheight, $titletype, $titleattr) = getimagesize('banners-content/images/' . $this->xml_banner->title->image->url);
			imagecopy($this->image, $imtitle, strval($this->xml_banner->title->image->position->left), strval($this->xml_banner->title->image->position->top), 0, 0, $titlewidth, $titleheight);
		}
		/**
		 * Add the title text
		 */
		foreach ($this->xml_banner->title->content as $textual) {
			$colors = $this->_rgb2array($textual->lang->color);
			$color = imagecolorallocate($this->image, $colors[0], $colors[1], $colors[2]);
			$trads = $textual;
			$text = "";
			foreach ($trads as $t) {
				if ((string) $t->attributes()->id == $this->lang) {
					$text = $t->value;
				}
			}
			
			$font = (string) $textual->lang->font . '.ttf';
			imagettftext($this->image, (int) $textual->lang->size * 0.7, 0, (int) $textual->lang->position->left, (int) $textual->lang->position->top + (int) $textual->lang->size * 0.7, $color, $font, $text);
		}
		/**
		 * Add the date
		 */
		$imdate = imagecreatefrompng('banners-content/images/' . $this->xml_banner->date->image->url);
		list ($datewidth, $dateheight, $datetype, $dateattr) = getimagesize('banners-content/images/' . $this->xml_banner->date->image->url);
		imagecopy($this->image, $imdate, strval($this->xml_banner->date->image->position->left), strval($this->xml_banner->date->image->position->top), 0, 0, $datewidth, $dateheight);
		$fromDateDisplay = (string) $this->xml_data->fromDate;
		$toDateDisplay = (string) $this->xml_data->toDate;
		
		$arr_date_params = $this->_set_date_to_display($fromDateDisplay, $toDateDisplay);
		
		foreach ($this->xml_banner->date->content as $textual) {
			$colors = $this->_rgb2array($textual->lang->color);
			$color = imagecolorallocate($this->image, $colors[0], $colors[1], $colors[2]);
			$text = $arr_date_params['dateDisplay2'];
			$font = (string) $textual->lang->font . '.ttf';
			
			$decal = 0;
			$bbox = imageftbbox((int) $textual->lang->size * 0.7, 0, $font, $text);
			$maxWidth = (int) $this->xml_banner->date->width;
			$currentWidth = $bbox[4] - $bbox[6];
			$decal = round(($maxWidth - $currentWidth) / 2, 0);
			if ($this->xml_banner->date->center != 'true') {
				$decal = 0;
			}
// 			if ((isset($this->xml_banner->date->formatLong) && ($this->xml_banner->date->formatLong == "true")) && ($this->lang == 'sl') && ($this->period != 'weekly') && ((($this->banner == "b468060") || ($this->banner == "b728090") || ($this->banner == "b655090")) && ($decalDate == 4))) {
// 				if ($this->banner == "b468060") {
// 					$imdate2 = imagecreatefrompng('banners-content/images/i468060-thursday-slovenia.png');
// 					list ($datewidth2, $dateheight2, $datetype2, $dateattr2) = getimagesize('banners-content/images/i468060-thursday-slovenia.png');
// 					imagecopy($this->image, $imdate2, - 17 + $decal + (int) $textual->lang->position->left, (int) $textual->lang->position->top + (int) $textual->lang->size * 0.7 - 8, 0, 0, $datewidth2, $dateheight2);
					
// 					imagettftext($this->image, (int) $textual->lang->size * 0.7, 0, 17 + $decal + (int) $textual->lang->position->left, (int) $textual->lang->position->top + (int) $textual->lang->size * 0.7, $color, $font, $text);
// 				}
// 				if ($this->banner == "b728090") {
// 					$imdate2 = imagecreatefrompng('banners-content/images/i728090-thursday-slovenia.png');
// 					list ($datewidth2, $dateheight2, $datetype2, $dateattr2) = getimagesize('banners-content/images/i728090-thursday-slovenia.png');
// 					imagecopy($this->image, $imdate2, - 26 + $decal + (int) $textual->lang->position->left, (int) $textual->lang->position->top + (int) $textual->lang->size * 0.7 - 11, 0, 0, $datewidth2, $dateheight2);
					
// 					imagettftext($this->image, (int) $textual->lang->size * 0.7, 0, 25 + $decal + (int) $textual->lang->position->left, (int) $textual->lang->position->top + (int) $textual->lang->size * 0.7, $color, $font, $text);
// 				}
// 			}
// 			else {
// 				imagettftext($this->image, (int) $textual->lang->size * 0.7, 0, $decal + (int) $textual->lang->position->left, (int) $textual->lang->position->top + (int) $textual->lang->size * 0.7, $color, $font, $text);
// 			}
// replaced by the following line and the use of UTF-8 in set locale for slovenian
			imagettftext($this->image, (int) $textual->lang->size * 0.7, 0, $decal + (int) $textual->lang->position->left, (int) $textual->lang->position->top + (int) $textual->lang->size * 0.7, $color, $font, $text);
				
			if ($arr_date_params['addon'] == 1) {
				$colors = $this->_rgb2array($textual->lang->color);
				$color = imagecolorallocate($this->image, $colors[0], $colors[1], $colors[2]);
				$text = $arr_date_params['dateDisplay2b'];
				
				$font = (string) $textual->lang->font . '.ttf';
				
				$decal = 0;
				$bbox = imageftbbox((int) $textual->lang->size * 0.7, 0, $font, $text);
				$maxWidth = (int) $this->xml_banner->date->width;
				$currentWidth = $bbox[4] - $bbox[6];
				$decal = round(($maxWidth - $currentWidth) / 2, 0);
				if ($this->xml_banner->date->center != 'true') {
					$decal = 0;
				}
				
				imagettftext($this->image, (int) $textual->lang->size * 0.7, 0, $decal + (int) $textual->lang->position->left + 8, (int) $textual->lang->position->top + (int) $textual->lang->size * 1.4 + 4, $color, $font, $text);
				
				$colors = $this->_rgb2array($textual->lang->color);
				$color = imagecolorallocate($this->image, $colors[0], $colors[1], $colors[2]);
				$text = $arr_date_params['dateDisplay2c'];
				$font = (string) $textual->lang->font . '.ttf';
				
				$decal = 0;
				$bbox = imageftbbox((int) $textual->lang->size * 0.7, 0, $font, $text);
				$maxWidth = (int) $this->xml_banner->date->width;
				$currentWidth = $bbox[4] - $bbox[6];
				$decal = round(($maxWidth - $currentWidth) / 2, 0);
				
				if ($this->xml_banner->date->center != 'true') {
					$decal = 0;
				}
				
				imagettftext($this->image, (int) $textual->lang->size * 0.7, 0, $decal + (int) $textual->lang->position->left, (int) $textual->lang->position->top + (int) $textual->lang->size * 2.1 + 8, $color, $font, $text);
			}
		}
		/**
		 * Add the town
		 */
		if ($this->xml_banner->town->image->url) {
			$imtown = imagecreatefrompng('banners-content/images/' . $this->xml_banner->town->image->url);
			list ($townwidth, $townheight, $towntype, $townattr) = getimagesize('banners-content/images/' . $this->xml_banner->town->image->url);
			imagecopy($this->image, $imtown, strval($this->xml_banner->town->image->position->left), strval($this->xml_banner->town->image->position->top), 0, 0, $townwidth, $townheight);
		}
		/**
		 * town text
		 */
		foreach ($this->xml_banner->town->content as $textual) {
			$colors = $this->_rgb2array($textual->lang->color);
			$color = imagecolorallocate($this->image, $colors[0], $colors[1], $colors[2]);
			$trads = $textual;
			$text = "";
			$textualtown = "";
			foreach ($trads as $t) {
				if ((string) $t->attributes()->id == $this->lang) {
					$text = $t->value;
					if ($text == '') {
						$textualtown = $textual;
					}
				}
			}
			$font = (string) $textual->lang->font . '.ttf';
			imagettftext($this->image, (int) $textual->lang->size * 0.7, 0, (int) $textual->lang->position->left, (int) $textual->lang->position->top + (int) $textual->lang->size * 0.7, $color, $font, $text);
		}
		/**
		 * Town data
		 */
		if ($textualtown != '') {
			$textual = $textualtown;
		}
		else {
			$textual = $this->xml_banner->town->content;
		}
		/**
		 * ? préparer la boite à texte
		 */
		$colors = $this->_rgb2array($textual->lang->color);
		$color = imagecolorallocate($this->image, $colors[0], $colors[1], $colors[2]);
		$text = $this->town;
		if ($this->country != 'slovenia') {
			$text = $this->_reduceTownName($text, 12);
		}
		$font = (string) $textual->lang->font . '.ttf';
		$decal = 0;
		$bbox = imageftbbox((int) $textual->lang->size * 0.7, 0, $font, $text);
		$maxWidth = (int) $this->xml_banner->town->width;
		$currentWidth = $bbox[4] - $bbox[6];
		$decal = round(($maxWidth - $currentWidth) / 2, 0);
		if ($textual->lang->center != 'true') {
			$decal = 0;
		}
		$text1 = "";
		$newsize = 0;
		$reduc = 0;
		if (($this->banner == 'b728090') && ($currentWidth > 90)) {
			
			if ($pos = strrpos($text, " ")) {
				$text1 = substr($text, 0, $pos);
				$text2 = substr($text, $pos + 1, strlen($text) - $pos);
				$text = $text1;
				$text1 = $text2;
			}
			else {
				$newsize = 1;
				if ($this->country == 'slovenia') {
					$reduc = 0;
				}
				else {
					$reduc = 2;
				}
			}
		}
		if (($this->banner == 'b468060') && ($currentWidth > 50)) {
			
			if ($pos = strrpos($text, " ")) {
				$text1 = substr($text, 0, $pos);
				$text2 = substr($text, $pos + 1, strlen($text) - $pos);
				$text = $text1;
				$text1 = $text2;
			}
			else {
				$newsize = 1;
				$reduc = 1;
			}
		}
		/**
		 * Ajouter le texte de la ville
		 */
		// $text = utf8_decode ( $townValue );
		$text = $townValue;
		$text = $this->_reduceTownName($text, 15);
		
		if ($newsize == 0) {
			imagettftext($this->image, (int) $textual->lang->size * 0.7, 0, $decal + (int) $textual->lang->position->left, (int) $textual->lang->position->top + (int) $textual->lang->size * 0.7, $color, $font, $text);
		}
		else {
			imagettftext($this->image, ((int) $textual->lang->size - $reduc) * 0.7, 0, $decal + (int) $textual->lang->position->left, (int) $textual->lang->position->top + ((int) $textual->lang->size - $reduc) * 0.7, $color, $font, $text);
		}
		if ($text1 != "") {
			$textual = $this->xml_banner->town->extra;
			imagettftext($this->image, (int) $textual->lang->size * 0.7, 0, $decal + (int) $textual->lang->position->left, (int) $textual->lang->position->top + (int) $textual->lang->size * 0.7, $color, $font, $text1);
		}
		/**
		 * add technology elements
		 */
		if (count($this->xml_banner->photovoltaique->children()) > 0) {
			$this->_add_element_pv();
		}
		
		if (count($this->xml_banner->eolien->children()) > 0) {
			$this->_add_element_wind();
		}
		
		if (count($this->xml_banner->thermique->children()) > 0) {
			$this->_add_element_soltherm();
		}
		
		return $this->image;
	}
}

/**
 * Here starts the code to build the banners
 */

$banner_image = new BannerImage($_GET);
$im = $banner_image->build_banner();
header("Content-Type: image/png; charset=utf-8");
error_reporting(E_ALL & ~ E_WARNING & ~ E_NOTICE);
imagepng($im);
imagedestroy($im);
return;
