<?
$country = $_GET ['country'];
if ($country != "slovenia") {
	
	header ( "Content-Type: image/png; charset=utf-8" );
	error_reporting ( E_ALL & ~ E_WARNING & ~ E_NOTICE );
	function rgb2array($rgb) {
		return array (
				base_convert ( substr ( $rgb, 0, 2 ), 16, 10 ),
				base_convert ( substr ( $rgb, 2, 2 ), 16, 10 ),
				base_convert ( substr ( $rgb, 4, 2 ), 16, 10 ) 
		);
	}
	function reduceTownName($chaine, $nb_car, $delim = '...') {
		$length = $nb_car;
		if ($nb_car < strlen ( $chaine )) {
			while ( ($chaine {$length} != " ") && ($length > 0) ) {
				$length --;
			}
			if ($length == 0)
				return substr ( $chaine, 0, $nb_car ) . $delim;
			else
				return substr ( $chaine, 0, $length ) . $delim;
		} else
			return $chaine;
	}
	
	$period = $_GET ['period'];
	$town = $_GET ['town'];
	$technos = $_GET ['technos'] ? $_GET ['technos'] : 'banners';
	$towns = explode ( ',', $town );
	if (count ( $towns ) > 1) {
		$town = $towns [rand ( 0, count ( $towns ) - 1 )];
	}
	
	$banner = $_GET ['format'];
	$lang = $_GET ['lang'];
	$error = 0;
	
	$xmlBan = file_get_contents ( $technos . '.xml' );
	
	$file = 'tmp/' . $country . '_' . $period . '.xml';
	$xmlData = file_get_contents ( $file );	
// 	$xmlData = file_get_contents ( 'http://ws.energizair.eu/wspartners/default?country=' . $country . '&period=' . $period . '&r=' . rand ( 1, 100000 ) );
	
	
	$xml = @simplexml_load_string ( $xmlBan );
	$xml2 = @simplexml_load_string ( $xmlData );
	
	$townTrad = null;
	$townValue = "";
	$townTradList = $xml2->PhotoVoltEnergy->solar_point;
	$nameLang = "name_" . $lang;
	foreach ( $townTradList as $t ) {
		if ((( string ) $t->name == $town) or (( string ) $t->name_en == $town)) {
			$townValue = $t->$nameLang;
		}
	}
	
	if (isset ( $xml->{$country} ))
		$subxml = $xml->{$country}->{$banner};
	else
		$subxml = $xml->default->{$banner};
		
	$sizes = explode ( 'x', $subxml->name );
	$width = $sizes [0];
	$height = $sizes [1];
	
	$background = $subxml->background->image->url;
	$im = imagecreatefrompng ( 'banners-content/images/' . $background );
	
	// logo
	$imlogo = imagecreatefrompng ( 'banners-content/images/' . $subxml->logo->image->url );
	list ( $logowidth, $logoheight, $logotype, $logoattr ) = getimagesize ( 'banners-content/images/' . $subxml->logo->image->url );
	imagecopy ( $im, $imlogo, strval ( $subxml->logo->image->position->left ), strval ( $subxml->logo->image->position->top ), 0, 0, $logowidth, $logoheight );
	
	// title
	if ($subxml->title->image->url) {
		$imtitle = imagecreatefrompng ( 'banners-content/images/' . $subxml->title->image->url );
		list ( $titlewidth, $titleheight, $titletype, $titleattr ) = getimagesize ( 'banners-content/images/' . $subxml->title->image->url );
		imagecopy ( $im, $imtitle, strval ( $subxml->title->image->position->left ), strval ( $subxml->title->image->position->top ), 0, 0, $titlewidth, $titleheight );
	}
	// title text
	foreach ( $subxml->title->content as $textual ) {
		$colors = rgb2array ( $textual->lang->color );
		$color = imagecolorallocate ( $im, $colors [0], $colors [1], $colors [2] );
		$trads = $textual;
		$text = "";
		foreach ( $trads as $t ) {
			if (( string ) $t->attributes ()->id == $lang) {
				$text = $t->value;
			}
		}
		
		$font = ( string ) $textual->lang->font . '.ttf';
		imagettftext ( $im, ( int ) $textual->lang->size * 0.7, 0, ( int ) $textual->lang->position->left, ( int ) $textual->lang->position->top + ( int ) $textual->lang->size * 0.7, $color, $font, $text );
	}
	
	// date
	$imdate = imagecreatefrompng ( 'banners-content/images/' . $subxml->date->image->url );
	list ( $datewidth, $dateheight, $datetype, $dateattr ) = getimagesize ( 'banners-content/images/' . $subxml->date->image->url );
	imagecopy ( $im, $imdate, strval ( $subxml->date->image->position->left ), strval ( $subxml->date->image->position->top ), 0, 0, $datewidth, $dateheight );
	// date text
	// setlocale(LC_ALL,$lang);
	if ($lang == 'pt')
		setlocale ( LC_ALL, 'pt_PT' );
	if ($lang == 'fr')
		setlocale ( LC_ALL, 'fr_FR' );
	if ($lang == 'en')
		setlocale ( LC_ALL, 'en_EN' );
	if ($lang == 'it')
		setlocale ( LC_ALL, 'it_IT' );
	if ($lang == 'sl')
		setlocale ( LC_ALL, 'sl_SI' );
	if ($lang == 'de')
		setlocale ( LC_ALL, 'de_DE' );
	if ($lang == 'hu')
		setlocale ( LC_ALL, 'hu_HU' );
	if ($lang == 'es')
		setlocale ( LC_ALL, 'es_ES' );
	if ($lang == 'ca')
		setlocale ( LC_ALL, 'es_CA' );
	if ($lang == 'sv')
		setlocale ( LC_ALL, 'sv_SE' );
	if ($lang == 'nl')
		setlocale ( LC_ALL, 'nl_NL' );
	
	$fromDateDisplay = ( string ) $xml2->fromDate;
	$toDateDisplay = ( string ) $xml2->toDate;
	
	$yearFrom = substr ( $fromDateDisplay, 0, 4 );
	$monthFrom = substr ( $fromDateDisplay, 5, 2 );
	$dayFrom = substr ( $fromDateDisplay, 8, 2 );
	
	$yearTo = substr ( $toDateDisplay, 0, 4 );
	$monthTo = substr ( $toDateDisplay, 5, 2 );
	$dayTo = substr ( $toDateDisplay, 8, 2 );
	
	$dateDisplay = ( string ) $xml2->toDate;
	$decalDate = date ( "N", strtotime ( $dateDisplay ) );
	$dateDisplay = date ( "Y-m-d", mktime ( "0", "0", "0", date ( "m", strtotime ( $dateDisplay ) ), date ( "d", strtotime ( $dateDisplay ) ) - 6, date ( "Y", strtotime ( $dateDisplay ) ) ) );
	
	$yearDisplay = substr ( $dateDisplay, 0, 4 );
	$monthDisplay = substr ( $dateDisplay, 5, 2 );
	$dayDisplay = substr ( $dateDisplay, 8, 2 );
	$startDay = $dayDisplay;
	$startDayChiffre = strtolower ( strftime ( "%d", mktime ( 0, 0, 0, 1, $dayDisplay, 1 ) ) );
	$startMonth = strtolower ( strftime ( "%b", mktime ( 0, 0, 0, $monthDisplay, 1, 1 ) ) );
	$startMonthN = strtolower ( strftime ( "%m", mktime ( 0, 0, 0, $monthDisplay, 1, 1 ) ) );
	$startMonthLong = strtolower ( strftime ( "%B", mktime ( 0, 0, 0, $monthDisplay, 1, 1 ) ) );
	$startYear = $yearDisplay;
	$endDay = date ( "d", mktime ( 0, 0, 0, $monthDisplay, $startDay + 6, $startYear ) );
	$endMonth = strtolower ( strftime ( "%b", mktime ( 0, 0, 0, $monthDisplay, $startDay + 6, $startYear ) ) );
	$endMonthN = strtolower ( strftime ( "%m", mktime ( 0, 0, 0, $monthDisplay, $startDay + 6, $startYear ) ) );
	$endMonthLong = strtolower ( strftime ( "%B", mktime ( 0, 0, 0, $monthDisplay, $startDay + 6, $startYear ) ) );
	$addon = 0;
	if ($period == "weekly") {
		// date week text
		$dateWeekText = $subxml->date->content->lang->value;
		foreach ( $subxml->date->content as $textual ) {
			$trads = $textual;
			foreach ( $trads as $t ) {
				if (( string ) $t->attributes ()->id == $lang) {
					$dateWeekText = $t->value;
				}
			}
		}
		
		if ($subxml->date->format == 'line') {
			$dateDisplay2 = $dateWeekText . " du " . $startDay . " " . $startMonth . " au " . $endDay . " " . $endMonth;
			if ($lang == 'fr') {
				$dateDisplay2 = $dateWeekText . " du " . $startDay . " " . $startMonth . " au " . $endDay . " " . $endMonth;
			}
			if ($lang == 'it') {
				$dateDisplay2 = $dateWeekText . " dal " . $startDay . " " . $startMonth . " al " . $endDay . " " . $endMonth;
			}
			if ($lang == 'sl') {
				switch ($banner) {
					case 'b120240' :
						if (isset ( $subxml->date->formatLong ) && ($subxml->date->formatLong == "true")) {
							$dateDisplay2 = $dateWeekText . " od " . $startDayChiffre . ". " . $startMonth . " do " . $endDay . ". " . $endMonth;
						} else {
							$dateDisplay2 = $dateWeekText . $startDayChiffre . ". " . $startMonth . " do " . $endDay . ". " . $endMonth;
						}
						break;
						
					case 'b468060' :
					case 'b655090' :
					case 'b728090' :
						if (isset ( $subxml->date->formatLong ) && ($subxml->date->formatLong == "true")) {
							$dateDisplay2 = $dateWeekText . " od " . $startDayChiffre . ". " . $startMonthLong . " do " . $endDay . ". " . $endMonthLong;
						} else {
							$dateDisplay2 = $dateWeekText . $startDayChiffre . ". " . $startMonthLong . " do " . $endDay . ". " . $endMonthLong;
						}
						break;
					case 'b160600' :
						if (isset ( $subxml->date->formatLong ) && ($subxml->date->formatLong == "true")) {
							$dateDisplay2 = $dateWeekText . " od " . $startDayChiffre . ". " . $startMonth . ($startMonth != "maj" ? "." : "") . " do " . $endDay . " " . $endMonth . "";
						} else {
							$dateDisplay2 = $dateWeekText . $startDayChiffre . ". " . $startMonth . ($startMonth != "maj" ? "." : "") . " do " . $endDay . ". " . $endMonth . ($endMonth != "maj" ? "." : "") . "";
						}
						break;						
			
					case 'b125125' :
						if (isset ( $subxml->date->formatLong ) && ($subxml->date->formatLong == "true")) {
							$dateDisplay2 = $dateWeekText . " od " . $startDayChiffre . ". " . $startMonth . " do " . $endDay . ". " . $endMonth;
						} else {
							$dateDisplay2 = $dateWeekText . $startDayChiffre . ". " . $startMonth . ($startMonth != "maj" ? "." : "") . " do " . $endDay . ". " . $endMonth . ($endMonth != "maj" ? "." : "") . "";
						}
						break;
				}
			}
			if ($lang == 'pt') {
				$dateDisplay2 = $dateWeekText . " de " . $startDay . " " . $startMonth . " a " . $endDay . " " . $endMonth;
			}
			if ($lang == 'en') {
				$dateDisplay2 = $dateWeekText . " " . $startDay . " " . ucfirst ( $startMonth ) . " to " . $endDay . " " . ucfirst ( $endMonth );
			}
			if ($lang == 'es') {
				$dateDisplay2 = $dateWeekText . " del " . $startDay . " " . $startMonth . " al " . $endDay . " " . $endMonth;
			}
			if ($lang == 'ca') {
				$dateDisplay2 = $dateWeekText . " del " . $startDay . " " . $startMonth . " al " . $endDay . " " . $endMonth;
			}
			if ($lang == 'de') {
				$dateDisplay2 = $dateWeekText . " " . $startDay . ". " . ucfirst ( $startMonth ) . " bis " . $endDay . ". " . ucfirst ( $endMonth );
			}
			if ($lang == 'hu') {
				$dateDisplay2 = $dateWeekText . " " . $startMonth . ". " . $startDay . ". - " . $endMonth . ". " . $endDay . ".";
			}
			if ($lang == 'sv') {
				$dateDisplay2 = $dateWeekText . " " . $startDay . " " . $startMonth . " till " . $endDay . " " . $endMonth;
			}
			if ($lang == 'nl') {
				$dateDisplay2 = $dateWeekText . " " . $startDay . " " . ucfirst ( $startMonth ) . " tot " . $endDay . " " . ucfirst ( $endMonth );
			}
		} else {
			$dateDisplay2 = $startDay . " " . $startMonth;
			if ($lang == "sl") {
				$dateDisplay2 = $startDay . ". " . $startMonth . ($startMonth != "maj" ? "." : "");
			}
			$dateDisplay2b = "au";
			$dateDisplay2c = $endDay . " " . $endMonth;
			if ($lang == "sl") {
				$dateDisplay2c = $endDay . ". " . $endMonth . ($endMonth != "maj" ? "." : "");
			}
			$addon = 1;
			if ($lang == 'it') {
				$dateDisplay2b = " al ";
			}
			if ($lang == 'sl') {
				{
					$dateDisplay2b = " do ";
				}
			}
			if ($lang == 'pt') {
				$dateDisplay2b = " a ";
			}
			if ($lang == 'en') {
				$dateDisplay2 = $startDay . " " . ucfirst ( $startMonth );
				$dateDisplay2b = " to ";
				$dateDisplay2c = $endDay . " " . ucfirst ( $endMonth );
			}
			if ($lang == 'sv') {
				$dateDisplay2b = " till ";
			}
			if ($lang == 'de') {
				$dateDisplay2 = $startDay . ". " . ucfirst ( $startMonth );
				$dateDisplay2b = " bis ";
				$dateDisplay2c = $endDay . ". " . ucfirst ( $endMonth );
			}
			if ($lang == 'es') {
				$dateDisplay2b = " al ";
			}
			if ($lang == 'ca') {
				$dateDisplay2b = " al ";
			}
			if ($lang == 'hu') {
				$dateDisplay2 = $startMonth . ". " . $startDay . ".";
				$dateDisplay2b = " - ";
				$dateDisplay2c = $endMonth . ". " . $endDay . ".";
			}
			if ($lang == 'nl') {
				$dateDisplay2 = $startDay . ". " . ucfirst ( $startMonth );
				$dateDisplay2b = " tot ";
				$dateDisplay2c = $endDay . ". " . ucfirst ( $endMonth );
			}
		}
	} else {
		if ($subxml->date->format == 'line') {
			$val_date_jour_1 = time () + (- 1) * 24 * 3600;
			$val_date_jour_1 = strtotime ( ( string ) $xml2->fromDate );
			if (isset ( $subxml->date->formatLong ) && ($subxml->date->formatLong == "true")) {
				$dateDisplay2 = (strftime ( "%A %d %B", $val_date_jour_1 ));
				if ($lang == 'sl') {
					$dateDisplay2 = (strftime ( "%A, %d. %B", $val_date_jour_1 ));
					if ((($banner == "b468060") || ($banner == "b728090") || ($banner == "b655090")) && ($decalDate == 4)) {
						$dateDisplay2 = (strftime ( ", %d. %B", $val_date_jour_1 ));
					}
				}
				if ($lang == 'hu') {
					$dateDisplay2 = (strftime ( "%B %d.", $val_date_jour_1 ));
				}
				if ($lang == 'pt') {
					$dateDisplay2 = (strftime ( "%A-feira %d %B", $val_date_jour_1 ));
				}
			} else {
				if (isset ( $subxml->date->formatLong ) && ($subxml->date->formatLong == "medium")) {
					$dateDisplay2 = (strftime ( "%d %B", $val_date_jour_1 ));
					if ($lang == 'sl') {
						$dateDisplay2 = (strftime ( "%d. %B", $val_date_jour_1 ));
					}
					if ($lang == 'hu') {
						$dateDisplay2 = (strftime ( "%B %d.", $val_date_jour_1 ));
					}
				} else {
					$dateDisplay2 = (strftime ( "%d %h", $val_date_jour_1 ));
					if ($lang == 'sl') {
						if (strftime ( "%h", $val_date_jour_1 ) != "maj")
							$dateDisplay2 = (strftime ( "%d. %h.", $val_date_jour_1 ));
						else
							$dateDisplay2 = (strftime ( "%d. %h", $val_date_jour_1 ));
					}
					if ($lang == 'hu') {
						$dateDisplay2 = (strftime ( "%h %d.", $val_date_jour_1 ));
					}
				}
			}
		} else {
			$val_date_jour_1 = time () + (- 1) * 24 * 3600;
			$val_date_jour_1 = strtotime ( ( string ) $xml2->fromDate );
			
			if (isset ( $subxml->date->formatLong ) && ($subxml->date->formatLong == "true")) {
				$dateDisplay2 = (strftime ( "%A %d %B", $val_date_jour_1 ));
				if ($lang == 'sl') {
					$dateDisplay2 = (strftime ( "%A, %d. %B", $val_date_jour_1 ));
					// $dateDisplay2='test';
				}
				if ($lang == 'hu') {
					$dateDisplay2 = (strftime ( "%B %d.", $val_date_jour_1 ));
				}
				if ($lang == 'pt') {
					$dateDisplay2 = (strftime ( "%A-feira %d %B", $val_date_jour_1 ));
				}
			} else {
				$dateDisplay2 = (strftime ( "%d %h", $val_date_jour_1 ));
				if ($lang == 'sl') {
					if (strftime ( "%h", $val_date_jour_1 ) != "maj")
						$dateDisplay2 = (strftime ( "%d. %h.", $val_date_jour_1 ));
					else
						$dateDisplay2 = (strftime ( "%d. %h", $val_date_jour_1 ));
				}
				if ($lang == 'hu') {
					$dateDisplay2 = (strftime ( "%h %d.", $val_date_jour_1 ));
				}
			}
			
			$dateDisplay2b = $dateDisplay2;
			$dateDisplay2 = "";
			$dateDisplay2c = "";
			$addon = 1;
		}
	}
	foreach ( $subxml->date->content as $textual ) {
		$colors = rgb2array ( $textual->lang->color );
		$color = imagecolorallocate ( $im, $colors [0], $colors [1], $colors [2] );
		$text = $dateDisplay2;
		$font = ( string ) $textual->lang->font . '.ttf';
		
		$decal = 0;
		$bbox = imageftbbox ( ( int ) $textual->lang->size * 0.7, 0, $font, $text );
		$maxWidth = ( int ) $subxml->date->width;
		$currentWidth = $bbox [4] - $bbox [6];
		$decal = round ( ($maxWidth - $currentWidth) / 2, 0 );
		if ($subxml->date->center != 'true')
			$decal = 0;
		if ((isset ( $subxml->date->formatLong ) && ($subxml->date->formatLong == "true")) && ($lang == 'sl') && ($period != 'weekly') && ((($banner == "b468060") || ($banner == "b728090")|| ($banner == "b655090")) && ($decalDate == 4))) {
			if ($banner == "b468060") {
				$imdate2 = imagecreatefrompng ( 'banners-content/images/i468060-thursday-slovenia.png' );
				list ( $datewidth2, $dateheight2, $datetype2, $dateattr2 ) = getimagesize ( 'banners-content/images/i468060-thursday-slovenia.png' );
				imagecopy ( $im, $imdate2, - 17 + $decal + ( int ) $textual->lang->position->left, ( int ) $textual->lang->position->top + ( int ) $textual->lang->size * 0.7 - 8, 0, 0, $datewidth2, $dateheight2 );
				
				imagettftext ( $im, ( int ) $textual->lang->size * 0.7, 0, 17 + $decal + ( int ) $textual->lang->position->left, ( int ) $textual->lang->position->top + ( int ) $textual->lang->size * 0.7, $color, $font, $text );
			}
			if ($banner == "b728090") {
				$imdate2 = imagecreatefrompng ( 'banners-content/images/i728090-thursday-slovenia.png' );
				list ( $datewidth2, $dateheight2, $datetype2, $dateattr2 ) = getimagesize ( 'banners-content/images/i728090-thursday-slovenia.png' );
				imagecopy ( $im, $imdate2, - 26 + $decal + ( int ) $textual->lang->position->left, ( int ) $textual->lang->position->top + ( int ) $textual->lang->size * 0.7 - 11, 0, 0, $datewidth2, $dateheight2 );
				
				imagettftext ( $im, ( int ) $textual->lang->size * 0.7, 0, 25 + $decal + ( int ) $textual->lang->position->left, ( int ) $textual->lang->position->top + ( int ) $textual->lang->size * 0.7, $color, $font, $text );
			}
		} else {
			imagettftext ( $im, ( int ) $textual->lang->size * 0.7, 0, $decal + ( int ) $textual->lang->position->left, ( int ) $textual->lang->position->top + ( int ) $textual->lang->size * 0.7, $color, $font, $text );
		}
		
		if ($addon == 1) {
			$colors = rgb2array ( $textual->lang->color );
			$color = imagecolorallocate ( $im, $colors [0], $colors [1], $colors [2] );
			$text = $dateDisplay2b;
			
			$font = ( string ) $textual->lang->font . '.ttf';
			
			$decal = 0;
			$bbox = imageftbbox ( ( int ) $textual->lang->size * 0.7, 0, $font, $text );
			$maxWidth = ( int ) $subxml->date->width;
			$currentWidth = $bbox [4] - $bbox [6];
			$decal = round ( ($maxWidth - $currentWidth) / 2, 0 );
			if ($subxml->date->center != 'true')
				$decal = 0;
			
			imagettftext ( $im, ( int ) $textual->lang->size * 0.7, 0, $decal + ( int ) $textual->lang->position->left + 8, ( int ) $textual->lang->position->top + ( int ) $textual->lang->size * 1.4 + 4, $color, $font, $text );
			
			$colors = rgb2array ( $textual->lang->color );
			$color = imagecolorallocate ( $im, $colors [0], $colors [1], $colors [2] );
			$text = $dateDisplay2c;
			$font = ( string ) $textual->lang->font . '.ttf';
			
			$decal = 0;
			$bbox = imageftbbox ( ( int ) $textual->lang->size * 0.7, 0, $font, $text );
			$maxWidth = ( int ) $subxml->date->width;
			$currentWidth = $bbox [4] - $bbox [6];
			$decal = round ( ($maxWidth - $currentWidth) / 2, 0 );
			
			if ($subxml->date->center != 'true')
				$decal = 0;
			
			imagettftext ( $im, ( int ) $textual->lang->size * 0.7, 0, $decal + ( int ) $textual->lang->position->left, ( int ) $textual->lang->position->top + ( int ) $textual->lang->size * 2.1 + 8, $color, $font, $text );
		}
	}
	
	// town
	if ($subxml->town->image->url) {
		$imtown = imagecreatefrompng ( 'banners-content/images/' . $subxml->town->image->url );
		list ( $townwidth, $townheight, $towntype, $townattr ) = getimagesize ( 'banners-content/images/' . $subxml->town->image->url );
		imagecopy ( $im, $imtown, strval ( $subxml->town->image->position->left ), strval ( $subxml->town->image->position->top ), 0, 0, $townwidth, $townheight );
	}
	// town text
	foreach ( $subxml->town->content as $textual ) {
		$colors = rgb2array ( $textual->lang->color );
		$color = imagecolorallocate ( $im, $colors [0], $colors [1], $colors [2] );
		
		$trads = $textual;
		$text = "";
		$textualtown = "";
		foreach ( $trads as $t ) {
			if (( string ) $t->attributes ()->id == $lang) {
				$text = $t->value;
				if ($text == '') {
					$textualtown = $textual;
				}
			}
		}
		
		$font = ( string ) $textual->lang->font . '.ttf';
		imagettftext ( $im, ( int ) $textual->lang->size * 0.7, 0, ( int ) $textual->lang->position->left, ( int ) $textual->lang->position->top + ( int ) $textual->lang->size * 0.7, $color, $font, $text );
	}
	// town data
	
	if ($textualtown != '')
		$textual = $textualtown;
	else
		$textual = $subxml->town->content;
	
	$colors = rgb2array ( $textual->lang->color );
	$color = imagecolorallocate ( $im, $colors [0], $colors [1], $colors [2] );
	$text = $town;
	$text = reduceTownName ( $text, 12 );
	if ($text == "italy")
		$text = "italia";
	$text = ucfirst ( $text );
	$font = ( string ) $textual->lang->font . '.ttf';
	$decal = 0;
	$bbox = imageftbbox ( ( int ) $textual->lang->size * 0.7, 0, $font, $text );
	$maxWidth = ( int ) $subxml->town->width;
	$currentWidth = $bbox [4] - $bbox [6];
	$decal = round ( ($maxWidth - $currentWidth) / 2, 0 );
	if ($textual->lang->center != 'true')
		$decal = 0;
	$text1 = "";
	$newsize = 0;
	$reduc = 0;
	if (($banner == 'b728090') && ($currentWidth > 90)) {
		
		if ($pos = strrpos ( $text, " " )) {
			$text1 = substr ( $text, 0, $pos );
			$text2 = substr ( $text, $pos + 1, strlen ( $text ) - $pos );
			$text = $text1;
			$text1 = $text2;
		} else {
			$newsize = 1;
			$reduc = 2;
		}
	}
	if (($banner == 'b468060') && ($currentWidth > 50)) {
		
		if ($pos = strrpos ( $text, " " )) {
			$text1 = substr ( $text, 0, $pos );
			$text2 = substr ( $text, $pos + 1, strlen ( $text ) - $pos );
			$text = $text1;
			$text1 = $text2;
		} else {
			$newsize = 1;
			$reduc = 1;
		}
	}
	
	$text = utf8_decode ( $townValue );
	$text = reduceTownName ( $text, 15 );
	
	if ($newsize == 0) {
		imagettftext ( $im, ( int ) $textual->lang->size * 0.7, 0, $decal + ( int ) $textual->lang->position->left, ( int ) $textual->lang->position->top + ( int ) $textual->lang->size * 0.7, $color, $font, $text );
	} else {
		imagettftext ( $im, (( int ) $textual->lang->size - $reduc) * 0.7, 0, $decal + ( int ) $textual->lang->position->left, ( int ) $textual->lang->position->top + (( int ) $textual->lang->size - $reduc) * 0.7, $color, $font, $text );
	}
	if ($text1 != "") {
		$textual = $subxml->town->extra;
		imagettftext ( $im, ( int ) $textual->lang->size * 0.7, 0, $decal + ( int ) $textual->lang->position->left, ( int ) $textual->lang->position->top + ( int ) $textual->lang->size * 0.7, $color, $font, $text1 );
	}
	
	if (count ( $subxml->photovoltaique->children () ) > 0) {
		// photovoltaique
		$imphotovoltaique = imagecreatefrompng ( 'banners-content/images/' . $subxml->photovoltaique->image->url );
		list ( $photovoltaiquewidth, $photovoltaiqueheight, $photovoltaiquetype, $photovoltaiqueattr ) = getimagesize ( 'banners-content/images/' . $subxml->photovoltaique->image->url );
		imagecopy ( $im, $imphotovoltaique, strval ( $subxml->photovoltaique->image->position->left ), strval ( $subxml->photovoltaique->image->position->top ), 0, 0, $photovoltaiquewidth, $photovoltaiqueheight );
		// photovoltaique text
		foreach ( $subxml->photovoltaique->content as $textual ) {
			$colors = rgb2array ( $textual->lang->color );
			$color = imagecolorallocate ( $im, $colors [0], $colors [1], $colors [2] );
			$trads = $textual;
			$text = "";
			foreach ( $trads as $t ) {
				if (( string ) $t->attributes ()->id == $lang) {
					$text = $t->value;
				}
			}
			$font = ( string ) $textual->lang->font . '.ttf';
			
			$decal = 0;
			$bbox = imageftbbox ( ( int ) $textual->lang->size * 0.7, 0, $font, $text );
			$maxWidth = ( int ) $textual->lang->width;
			$currentWidth = $bbox [4] - $bbox [6];
			$decal = round ( ($maxWidth - $currentWidth) / 2, 0 );
			if ($textual->lang->center != 'true')
				$decal = 0;
			imagettftext ( $im, ( int ) $textual->lang->size * 0.7, 0, $decal + ( int ) $textual->lang->position->left, ( int ) $textual->lang->position->top + ( int ) $textual->lang->size * 0.7, $color, $font, $text );
		}
		// photovolta data
		$dataValue = null;
		$dataValueList = $xml2->PhotoVoltEnergy->solar_point;
		foreach ( $dataValueList as $t ) {
			if ((( string ) $t->name == $town) or (( string ) $t->name_en == $town)) {
				$dataValue = $t;
			}
		}
		
		$textual = $subxml->photovoltaique->data;
		$colors = rgb2array ( $textual->lang->color );
		$color = imagecolorallocate ( $im, $colors [0], $colors [1], $colors [2] );
		if ($period == 'daily') {
			$text = $dataValue->ElectricNeedscoverage . "%";
			if (($country == 'belgium') || ($country == 'italy') || ($country == 'slovenia') || ($country == 'portugal') || ($country == 'france') || ($country == 'sweden') || ($country == 'hungary') || ($country == 'germany') || ($country == 'spain') || ($country == 'united_kingdom'))
				$text = $dataValue->NeedscoveragePV . "%";
		} else {
			$text = $dataValue->NeedscoveragePV . "%";
		}
		$font = ( string ) $textual->lang->font . '.ttf';
		
		$decal = 0;
		$bbox = imageftbbox ( ( int ) $textual->lang->size * 0.7, 0, $font, $text );
		$maxWidth = ( int ) $textual->lang->width;
		$currentWidth = $bbox [4] - $bbox [6];
		$decal = round ( ($maxWidth - $currentWidth) / 2, 0 );
		if ($textual->lang->center != 'true')
			$decal = 0;
		imagettftext ( $im, ( int ) $textual->lang->size * 0.7, 0, $decal + ( int ) $textual->lang->position->left, ( int ) $textual->lang->position->top + ( int ) $textual->lang->size * 0.7, $color, $font, $text );
	}
	
	if (count ( $subxml->eolien->children () ) > 0) {
		// eolien
		
		$imeolien = imagecreatefrompng ( 'banners-content/images/' . $subxml->eolien->image->url );
		list ( $eolienwidth, $eolienheight, $eolientype, $eolienattr ) = getimagesize ( 'banners-content/images/' . $subxml->eolien->image->url );
		imagecopy ( $im, $imeolien, strval ( $subxml->eolien->image->position->left ), strval ( $subxml->eolien->image->position->top ), 0, 0, $eolienwidth, $eolienheight );
		// eolien text
		$cptx = 0;
		foreach ( $subxml->eolien->content as $textual ) {
			$colors = rgb2array ( $textual->lang->color );
			$color = imagecolorallocate ( $im, $colors [0], $colors [1], $colors [2] );
			$trads = $textual;
			$text = "";
			
			foreach ( $trads as $t ) {
				$cptx ++;
				if (( string ) $t->attributes ()->id == $lang) {
					$text = $t->value;
				}
				
				$dataValue = null;
				$dataValueList = $xml2->PhotoVoltEnergy->solar_point;
				foreach ( $dataValueList as $t ) {
					if ((( string ) $t->name == $town) or (( string ) $t->name_en == $town)) {
						$dataValue = $t;
					}
				}
				if (($cptx == 1) && ($text != '') && ($banner != "b125125") && ($dataValue->display_nat_ref == "1")) {
					if ($country == 'belgium')
						$text .= " (BE)";
					if ($country == 'italy')
						$text .= " (IT)";
					if ($country == 'slovenia')
						$text .= " (SL)";
					if ($country == 'portugal')
						$text .= " (PT)";
					if ($country == 'france')
						$text .= " (FR)";
				}
			}
			$font = ( string ) $textual->lang->font . '.ttf';
			
			$decal = 0;
			$bbox = imageftbbox ( ( int ) $textual->lang->size * 0.7, 0, $font, $text );
			$maxWidth = ( int ) $textual->lang->width;
			$currentWidth = $bbox [4] - $bbox [6];
			$decal = round ( ($maxWidth - $currentWidth) / 2, 0 );
			if ($textual->lang->center != 'true')
				$decal = 0;
			imagettftext ( $im, ( int ) $textual->lang->size * 0.7, 0, $decal + ( int ) $textual->lang->position->left, ( int ) $textual->lang->position->top + ( int ) $textual->lang->size * 0.7, $color, $font, $text );
		}
		// eolien data
		$dataValue = null;
		$dataValueList = $xml2->PhotoVoltEnergy->solar_point;
		foreach ( $dataValueList as $t ) {
			if ((( string ) $t->name == $town) or (( string ) $t->name_en == $town)) {
				$dataValue = $t;
			}
		}
		$area = $dataValue->parentwindarea;
		
		$dataValue = null;
		$dataValueList = $xml2->WindEnergy->Areas;
		
		foreach ( $dataValueList->Area as $t ) {
			if (strtolower ( ( string ) $t->name ) == strtolower ( $area )) {
				$dataValue = $t;
				break;
			}
		}
		
		$textual = $subxml->eolien->data;
		$colors = rgb2array ( $textual->lang->color );
		$color = imagecolorallocate ( $im, $colors [0], $colors [1], $colors [2] );
		// die($_GET['country']." ".strtolower((string)$t->name)." ".print_r($dataValue,true)." ".print_r($dataValueList,true));
		$text = number_format ( $dataValue->NumberOfHouses, 0, ",", "." );
		// $text="Unavailable";
		/*
		 * if ($lang=="fr") $text = "Indisponible"; if ($lang=="it") $text="Non disponibile"; if ($lang=="pt") $text="Indisponível";
		 */
		$font = ( string ) $textual->lang->font . '.ttf';
		$decal = 0;
		$bbox = imageftbbox ( ( int ) $textual->lang->size * 0.7, 0, $font, $text );
		$maxWidth = ( int ) $textual->lang->width;
		$currentWidth = $bbox [4] - $bbox [6];
		$decal = round ( ($maxWidth - $currentWidth) / 2, 0 );
		if ($textual->lang->center != 'true')
			$decal = 0;
		imagettftext ( $im, ( int ) $textual->lang->size * 0.7, 0, $decal + ( int ) $textual->lang->position->left, ( int ) $textual->lang->position->top + ( int ) $textual->lang->size * 0.7, $color, $font, $text );
	}
	
	if (count ( $subxml->thermique->children () ) > 0) {
		// thermique
		$imthermique = imagecreatefrompng ( 'banners-content/images/' . $subxml->thermique->image->url );
		list ( $thermiquewidth, $thermiqueheight, $thermiquetype, $thermiqueattr ) = getimagesize ( 'banners-content/images/' . $subxml->thermique->image->url );
		imagecopy ( $im, $imthermique, strval ( $subxml->thermique->image->position->left ), strval ( $subxml->thermique->image->position->top ), 0, 0, $thermiquewidth, $thermiqueheight );
		// thermique text
		foreach ( $subxml->thermique->content as $textual ) {
			$colors = rgb2array ( $textual->lang->color );
			$color = imagecolorallocate ( $im, $colors [0], $colors [1], $colors [2] );
			$trads = $textual;
			$text = "";
			foreach ( $trads as $t ) {
				if (( string ) $t->attributes ()->id == $lang) {
					$text = $t->value;
				}
			}
			$font = ( string ) $textual->lang->font . '.ttf';
			$decal = 0;
			$bbox = imageftbbox ( ( int ) $textual->lang->size * 0.7, 0, $font, $text );
			$maxWidth = ( int ) $textual->lang->width;
			$currentWidth = $bbox [4] - $bbox [6];
			$decal = round ( ($maxWidth - $currentWidth) / 2, 0 );
			if ($textual->lang->center != 'true')
				$decal = 0;
			imagettftext ( $im, ( int ) $textual->lang->size * 0.7, 0, $decal + ( int ) $textual->lang->position->left, ( int ) $textual->lang->position->top + ( int ) $textual->lang->size * 0.7, $color, $font, $text );
		}
		// thermique data
		$dataValue = null;
		$dataValueList = $xml2->SolarTherm->solar_point;
		foreach ( $dataValueList as $t ) {
			if ((( string ) $t->name == $town) or (( string ) $t->name_en == $town)) {
				$dataValue = $t;
			}
		}
		
		$textual = $subxml->thermique->data;
		$colors = rgb2array ( $textual->lang->color );
		$color = imagecolorallocate ( $im, $colors [0], $colors [1], $colors [2] );
		
		if ($period == 'daily') {
			
			$text = $dataValue->ThermalNeedscoverage . "%";
			if (($country == 'belgium') || ($country == 'italy') || ($country == 'slovenia') || ($country == 'portugal') || ($country == 'france') || ($country == 'sweden') || ($country == 'hungary') || ($country == 'germany') || ($country == 'spain') || ($country == 'united_kingdom'))
				$text = $dataValue->NeedscoverageTH . "%";
		} else {
			$text = $dataValue->NeedscoverageTH . "%";
		}
		// $text = $dataValue->NeedscoverageTH."%";
		$font = ( string ) $textual->lang->font . '.ttf';
		
		$decal = 0;
		$bbox = imageftbbox ( ( int ) $textual->lang->size * 0.7, 0, $font, $text );
		$maxWidth = ( int ) $textual->lang->width;
		$currentWidth = $bbox [4] - $bbox [6];
		$decal = round ( ($maxWidth - $currentWidth) / 2, 0 );
		if ($textual->lang->center != 'true')
			$decal = 0;
		imagettftext ( $im, ( int ) $textual->lang->size * 0.7, 0, $decal + ( int ) $textual->lang->position->left, ( int ) $textual->lang->position->top + ( int ) $textual->lang->size * 0.7, $color, $font, $text );
	}
	
	imagepng ( $im );
	imagedestroy ( $im );
} else {
	header ( "Content-Type: image/png" );
	error_reporting ( E_ALL & ~ E_WARNING & ~ E_NOTICE );
	function rgb2array($rgb) {
		return array (
				base_convert ( substr ( $rgb, 0, 2 ), 16, 10 ),
				base_convert ( substr ( $rgb, 2, 2 ), 16, 10 ),
				base_convert ( substr ( $rgb, 4, 2 ), 16, 10 ) 
		);
	}
	
	$country = $_GET ['country'];
	if ($country != "belgium") {
		
		$period = $_GET ['period'];
		$town = $_GET ['town'];
		$technos = $_GET ['technos'] ? $_GET ['technos'] : 'banners';
		if (($town == "") && ($country == "portugal"))
			$town = "Lisboa";
		$towns = explode ( ',', $town );
		if (count ( $towns ) > 1) {
			$town = $towns [rand ( 0, count ( $towns ) - 1 )];
		}
		
		$banner = $_GET ['format'];
		$lang = $_GET ['lang'];
		$error = 0;
		
		$xmlBan = file_get_contents ( $technos . '.xml' );
		
		$file = 'tmp/' . $country . '_' . $period . '.xml';
		$xmlData = file_get_contents ( $file );
// 		$xmlData = file_get_contents ( 'http://ws.energizair.eu/wspartners/default?country=' . $country . '&period=' . $period . '&r=' . rand ( 1, 100000 ) );
		
		$xml = @simplexml_load_string ( $xmlBan );
		$xml2 = @simplexml_load_string ( $xmlData );
		
		// echo print_r($xml2,true);
		
		if (isset ( $xml->{$country} ))
			$subxml = $xml->{$country}->{$banner};
		else
			$subxml = $xml->default->{$banner};
			
			// ADD TOWN SL
		$townTrad = null;
		$townTradList = $xml2->PhotoVoltEnergy->solar_point;
		$nameLang = "name_" . $lang;
		foreach ( $townTradList as $t ) {
			if ((( string ) $t->name == $town) or (( string ) $t->name_en == $town)) {
				$townValue = $t->$nameLang;
			}
		}
		// -----------
		
		// $subxml=$subxml->{$banner};
		// echo ('c: '.$country.' b: '.$banner);
		$sizes = split ( 'x', $subxml->name );
		$width = $sizes [0];
		$height = $sizes [1];
		
		$background = $subxml->background->image->url;
		$im = imagecreatefrompng ( 'banners-content/images/' . $background );
		
		// logo
		$imlogo = imagecreatefrompng ( 'banners-content/images/' . $subxml->logo->image->url );
		list ( $logowidth, $logoheight, $logotype, $logoattr ) = getimagesize ( 'banners-content/images/' . $subxml->logo->image->url );
		imagecopy ( $im, $imlogo, $subxml->logo->image->position->left, $subxml->logo->image->position->top, 0, 0, $logowidth, $logoheight );
		
		// title
		if ($subxml->title->image->url) {
			$imtitle = imagecreatefrompng ( 'banners-content/images/' . $subxml->title->image->url );
			list ( $titlewidth, $titleheight, $titletype, $titleattr ) = getimagesize ( 'banners-content/images/' . $subxml->title->image->url );
			imagecopy ( $im, $imtitle, $subxml->title->image->position->left, $subxml->title->image->position->top, 0, 0, $titlewidth, $titleheight );
		}
		// title text
		foreach ( $subxml->title->content as $textual ) {
			$colors = rgb2array ( $textual->lang->color );
			$color = imagecolorallocate ( $im, $colors [0], $colors [1], $colors [2] );
			$trads = $textual;
			$text = "";
			foreach ( $trads as $t ) {
				if (( string ) $t->attributes ()->id == $lang) {
					$text = $t->value;
				}
			}
			
			$font = ( string ) $textual->lang->font . '.ttf';
			imagettftext ( $im, ( int ) $textual->lang->size * 0.7, 0, ( int ) $textual->lang->position->left, ( int ) $textual->lang->position->top + ( int ) $textual->lang->size * 0.7, $color, $font, $text );
		}
		
		// date
		$imdate = imagecreatefrompng ( 'banners-content/images/' . $subxml->date->image->url );
		list ( $datewidth, $dateheight, $datetype, $dateattr ) = getimagesize ( 'banners-content/images/' . $subxml->date->image->url );
		imagecopy ( $im, $imdate, $subxml->date->image->position->left, $subxml->date->image->position->top, 0, 0, $datewidth, $dateheight );
		// date text
		// setlocale(LC_ALL,$lang);
		if ($lang == 'pt')
			setlocale ( LC_ALL, 'pt_PT' );
		if ($lang == 'fr')
			setlocale ( LC_ALL, 'fr_FR' );
		if ($lang == 'en')
			setlocale ( LC_ALL, 'en_EN' );
		if ($lang == 'it')
			setlocale ( LC_ALL, 'it_IT' );
		if ($lang == 'sl')
			setlocale ( LC_ALL, 'sl_SI' );
		if ($lang == 'de')
			setlocale ( LC_ALL, 'de_DE' );
		if ($lang == 'hu')
			setlocale ( LC_ALL, 'hu_HU' );
		if ($lang == 'es')
			setlocale ( LC_ALL, 'es_ES' );
		if ($lang == 'ca')
			setlocale ( LC_ALL, 'es_CA' );
		if ($lang == 'sv')
			setlocale ( LC_ALL, 'sv_SE' );
		if ($lang == 'nl')
			setlocale ( LC_ALL, 'nl_NL' );
		
		$dateDisplay = ( string ) $xml2->toDate;
		$decalDate = date ( "N", strtotime ( $dateDisplay ) );
		$dateDisplay = date ( "Y-m-d", mktime ( "0", "0", "0", date ( "m", strtotime ( $dateDisplay ) ), date ( "d", strtotime ( $dateDisplay ) ) - 6, date ( "Y", strtotime ( $dateDisplay ) ) ) );
		
		$yearDisplay = substr ( $dateDisplay, 0, 4 );
		$monthDisplay = substr ( $dateDisplay, 5, 2 );
		$dayDisplay = substr ( $dateDisplay, 8, 2 );
		$startDay = $dayDisplay;
		$startDayChiffre = strtolower ( strftime ( "%d", mktime ( 0, 0, 0, 1, $dayDisplay, 1 ) ) );
		$startMonth = strtolower ( strftime ( "%b", mktime ( 0, 0, 0, $monthDisplay, 1, 1 ) ) );
		$startMonthN = strtolower ( strftime ( "%m", mktime ( 0, 0, 0, $monthDisplay, 1, 1 ) ) );
		$startMonthLong = strtolower ( strftime ( "%B", mktime ( 0, 0, 0, $monthDisplay, 1, 1 ) ) );
		$startYear = $yearDisplay;
		$endDay = date ( "d", mktime ( 0, 0, 0, $monthDisplay, $startDay + 6, $startYear ) );
		$endMonth = strtolower ( strftime ( "%b", mktime ( 0, 0, 0, $monthDisplay, $startDay + 6, $startYear ) ) );
		$endMonthN = strtolower ( strftime ( "%m", mktime ( 0, 0, 0, $monthDisplay, $startDay + 6, $startYear ) ) );
		$endMonthLong = strtolower ( strftime ( "%B", mktime ( 0, 0, 0, $monthDisplay, $startDay + 6, $startYear ) ) );
		$addon = 0;
		if ($period == "weekly") {
			// date week text
			$dateWeekText = $subxml->date->content->lang->value;
			foreach ( $subxml->date->content as $textual ) {
				$trads = $textual;
				foreach ( $trads as $t ) {
					if (( string ) $t->attributes ()->id == $lang) {
						$dateWeekText = $t->value;
					}
				}
			}
			
			if ($subxml->date->format == 'line') {
				$dateDisplay2 = $dateWeekText . " du " . $startDay . " " . $startMonth . " au " . $endDay . " " . $endMonth;
				if ($lang == 'fr') {
					$dateDisplay2 = $dateWeekText . " du " . $startDay . " " . $startMonth . " au " . $endDay . " " . $endMonth;
				}
				if ($lang == 'it') {
					$dateDisplay2 = $dateWeekText . " dal " . $startDay . " " . $startMonth . " al " . $endDay . " " . $endMonth;
				}
				if ($lang == 'sl') {
					switch ($banner) {
						case 'b120240' :
						case 'b655090' :
							if (isset ( $subxml->date->formatLong ) && ($subxml->date->formatLong == "true")) {
								$dateDisplay2 = $dateWeekText . " od " . $startDayChiffre . ". " . $startMonth . " do " . $endDay . ". " . $endMonth;
							} else {
								$dateDisplay2 = $dateWeekText . $startDayChiffre . ". " . $startMonth . " do " . $endDay . ". " . $endMonth;
							}
							break;
							
						case 'b468060' :
						case 'b728090' :
							if (isset ( $subxml->date->formatLong ) && ($subxml->date->formatLong == "true")) {
								$dateDisplay2 = $dateWeekText . " od " . $startDayChiffre . ". " . $startMonthLong . " do " . $endDay . ". " . $endMonthLong;
							} else {
								$dateDisplay2 = $dateWeekText . $startDayChiffre . ". " . $startMonthLong . " do " . $endDay . ". " . $endMonthLong;
							}
							break;
							
						case 'b160600' :
							if (isset ( $subxml->date->formatLong ) && ($subxml->date->formatLong == "true")) {
								$dateDisplay2 = $dateWeekText . " od " . $startDayChiffre . ". " . $startMonth . ($startMonth != "maj" ? "." : "") . " do " . $endDay . " " . $endMonth . "";
							} else {
								$dateDisplay2 = $dateWeekText . $startDayChiffre . ". " . $startMonth . ($startMonth != "maj" ? "." : "") . " do " . $endDay . ". " . $endMonth . ($endMonth != "maj" ? "." : "") . "";
							}
							break;
						
						case 'b125125' :
							if (isset ( $subxml->date->formatLong ) && ($subxml->date->formatLong == "true")) {
								$dateDisplay2 = $dateWeekText . " od " . $startDayChiffre . ". " . $startMonth . " do " . $endDay . ". " . $endMonth;
							} else {
								$dateDisplay2 = $dateWeekText . $startDayChiffre . ". " . $startMonth . ($startMonth != "maj" ? "." : "") . " do " . $endDay . ". " . $endMonth . ($endMonth != "maj" ? "." : "") . "";
							}
							break;
					}
				}
				if ($lang == 'pt') {
					$dateDisplay2 = $dateWeekText . " de " . $startDay . " " . $startMonth . " a " . $endDay . " " . $endMonth;
				}
			} else {
				$dateDisplay2 = $startDay . " " . $startMonth;
				if ($lang == "sl") {
					$dateDisplay2 = $startDay . ". " . $startMonth . ($startMonth != "maj" ? "." : "");
				}
				$dateDisplay2b = "au";
				$dateDisplay2c = $endDay . " " . $endMonth;
				if ($lang == "sl") {
					$dateDisplay2c = $endDay . ". " . $endMonth . ($endMonth != "maj" ? "." : "");
				}
				$addon = 1;
				if ($lang == 'it') {
					$dateDisplay2b = " al ";
				}
				if ($lang == 'sl') {
					{
						$dateDisplay2b = " do ";
					}
				}
				if ($lang == 'pt') {
					$dateDisplay2b = " a ";
				}
				if ($lang == 'en') {
					$dateDisplay2b = " to ";
				}
				if ($lang == 'sv') {
					$dateDisplay2b = " till ";
				}
				if ($lang == 'de') {
					$dateDisplay2b = " bis ";
				}
				if ($lang == 'es') {
					$dateDisplay2b = " al ";
				}
				if ($lang == 'ca') {
					$dateDisplay2b = " al ";
				}
				if ($lang == 'nl') {
					$dateDisplay2b = " tot ";
				}
			}
		} else {
			if ($subxml->date->format == 'line') {
				$val_date_jour_1 = time () + (- 1) * 24 * 3600;
				$val_date_jour_1 = strtotime ( ( string ) $xml2->fromDate );
				if (isset ( $subxml->date->formatLong ) && ($subxml->date->formatLong == "true")) {
					$dateDisplay2 = (strftime ( "%A %d %B", $val_date_jour_1 ));
					if ($lang == 'sl') {
						$dateDisplay2 = (strftime ( "%A, %d. %B", $val_date_jour_1 ));
						if ((($banner == "b468060") || ($banner == "b728090") || ($banner == "b655090")) && ($decalDate == 4)) {
							$dateDisplay2 = (strftime ( ", %d. %B", $val_date_jour_1 ));
						}
					}
					if ($lang == 'pt') {
						$dateDisplay2 = (strftime ( "%A-feira %d %B", $val_date_jour_1 ));
					}
				} else {
					if (isset ( $subxml->date->formatLong ) && ($subxml->date->formatLong == "medium")) {
						$dateDisplay2 = (strftime ( "%d %B", $val_date_jour_1 ));
						if ($lang == 'sl') {
							$dateDisplay2 = (strftime ( "%d. %B", $val_date_jour_1 ));
						}
					} else {
						$dateDisplay2 = (strftime ( "%d %h", $val_date_jour_1 ));
						if ($lang == 'sl') {
							if (strftime ( "%h", $val_date_jour_1 ) != "maj")
								$dateDisplay2 = (strftime ( "%d. %h.", $val_date_jour_1 ));
							else
								$dateDisplay2 = (strftime ( "%d. %h", $val_date_jour_1 ));
						}
					}
				}
			} else {
				$val_date_jour_1 = time () + (- 1) * 24 * 3600;
				$val_date_jour_1 = strtotime ( ( string ) $xml2->fromDate );
				
				if (isset ( $subxml->date->formatLong ) && ($subxml->date->formatLong == "true")) {
					$dateDisplay2 = (strftime ( "%A %d %B", $val_date_jour_1 ));
					if ($lang == 'sl') {
						$dateDisplay2 = (strftime ( "%A, %d. %B", $val_date_jour_1 ));
					}
					if ($lang == 'pt') {
						$dateDisplay2 = (strftime ( "%A-feira %d %B", $val_date_jour_1 ));
					}
				} else {
					$dateDisplay2 = (strftime ( "%d %h", $val_date_jour_1 ));
					if ($lang == 'sl') {
						if (strftime ( "%h", $val_date_jour_1 ) != "maj")
							$dateDisplay2 = (strftime ( "%d. %h.", $val_date_jour_1 ));
						else
							$dateDisplay2 = (strftime ( "%d. %h", $val_date_jour_1 ));
					}
				}
				
				$dateDisplay2b = $dateDisplay2;
				$dateDisplay2 = "";
				$dateDisplay2c = "";
				$addon = 1;
			}
		}
		foreach ( $subxml->date->content as $textual ) {
			$colors = rgb2array ( $textual->lang->color );
			$color = imagecolorallocate ( $im, $colors [0], $colors [1], $colors [2] );
			$text = $dateDisplay2;
			$font = ( string ) $textual->lang->font . '.ttf';
			
			$decal = 0;
			$bbox = imageftbbox ( ( int ) $textual->lang->size * 0.7, 0, $font, $text );
			$maxWidth = ( int ) $subxml->date->width;
			$currentWidth = $bbox [4] - $bbox [6];
			$decal = round ( ($maxWidth - $currentWidth) / 2, 0 );
			if ($subxml->date->center != 'true')
				$decal = 0;
			if ((isset ( $subxml->date->formatLong ) && ($subxml->date->formatLong == "true")) && ($lang == 'sl') && ($period != 'weekly') && ((($banner == "b468060") || ($banner == "b728090") || ($banner == "b655090")) && ($decalDate == 4))) {
				if ($banner == "b468060") {
					$imdate2 = imagecreatefrompng ( 'banners-content/images/i468060-thursday-slovenia.png' );
					list ( $datewidth2, $dateheight2, $datetype2, $dateattr2 ) = getimagesize ( 'banners-content/images/i468060-thursday-slovenia.png' );
					imagecopy ( $im, $imdate2, - 17 + $decal + ( int ) $textual->lang->position->left, ( int ) $textual->lang->position->top + ( int ) $textual->lang->size * 0.7 - 8, 0, 0, $datewidth2, $dateheight2 );
					
					imagettftext ( $im, ( int ) $textual->lang->size * 0.7, 0, 17 + $decal + ( int ) $textual->lang->position->left, ( int ) $textual->lang->position->top + ( int ) $textual->lang->size * 0.7, $color, $font, $text );
				}
				if ($banner == "b728090") {
					$imdate2 = imagecreatefrompng ( 'banners-content/images/i728090-thursday-slovenia.png' );
					list ( $datewidth2, $dateheight2, $datetype2, $dateattr2 ) = getimagesize ( 'banners-content/images/i728090-thursday-slovenia.png' );
					imagecopy ( $im, $imdate2, - 26 + $decal + ( int ) $textual->lang->position->left, ( int ) $textual->lang->position->top + ( int ) $textual->lang->size * 0.7 - 11, 0, 0, $datewidth2, $dateheight2 );
					
					imagettftext ( $im, ( int ) $textual->lang->size * 0.7, 0, 25 + $decal + ( int ) $textual->lang->position->left, ( int ) $textual->lang->position->top + ( int ) $textual->lang->size * 0.7, $color, $font, $text );
				}
			} else {
				imagettftext ( $im, ( int ) $textual->lang->size * 0.7, 0, $decal + ( int ) $textual->lang->position->left, ( int ) $textual->lang->position->top + ( int ) $textual->lang->size * 0.7, $color, $font, $text );
			}
			
			if ($addon == 1) {
				$colors = rgb2array ( $textual->lang->color );
				$color = imagecolorallocate ( $im, $colors [0], $colors [1], $colors [2] );
				$text = $dateDisplay2b;
				
				$font = ( string ) $textual->lang->font . '.ttf';
				
				$decal = 0;
				$bbox = imageftbbox ( ( int ) $textual->lang->size * 0.7, 0, $font, $text );
				$maxWidth = ( int ) $subxml->date->width;
				$currentWidth = $bbox [4] - $bbox [6];
				$decal = round ( ($maxWidth - $currentWidth) / 2, 0 );
				if ($subxml->date->center != 'true')
					$decal = 0;
				
				imagettftext ( $im, ( int ) $textual->lang->size * 0.7, 0, $decal + ( int ) $textual->lang->position->left + 8, ( int ) $textual->lang->position->top + ( int ) $textual->lang->size * 1.4 + 4, $color, $font, $text );
				
				$colors = rgb2array ( $textual->lang->color );
				$color = imagecolorallocate ( $im, $colors [0], $colors [1], $colors [2] );
				$text = $dateDisplay2c;
				$font = ( string ) $textual->lang->font . '.ttf';
				
				$decal = 0;
				$bbox = imageftbbox ( ( int ) $textual->lang->size * 0.7, 0, $font, $text );
				$maxWidth = ( int ) $subxml->date->width;
				$currentWidth = $bbox [4] - $bbox [6];
				$decal = round ( ($maxWidth - $currentWidth) / 2, 0 );
				
				if ($subxml->date->center != 'true')
					$decal = 0;
				
				imagettftext ( $im, ( int ) $textual->lang->size * 0.7, 0, $decal + ( int ) $textual->lang->position->left, ( int ) $textual->lang->position->top + ( int ) $textual->lang->size * 2.1 + 8, $color, $font, $text );
			}
		}
		
		// town
		if ($subxml->town->image->url) {
			$imtown = imagecreatefrompng ( 'banners-content/images/' . $subxml->town->image->url );
			list ( $townwidth, $townheight, $towntype, $townattr ) = getimagesize ( 'banners-content/images/' . $subxml->town->image->url );
			imagecopy ( $im, $imtown, $subxml->town->image->position->left, $subxml->town->image->position->top, 0, 0, $townwidth, $townheight );
		}
		// town text
		foreach ( $subxml->town->content as $textual ) {
			$colors = rgb2array ( $textual->lang->color );
			$color = imagecolorallocate ( $im, $colors [0], $colors [1], $colors [2] );
			
			$trads = $textual;
			$text = "";
			$textualtown = "";
			foreach ( $trads as $t ) {
				if (( string ) $t->attributes ()->id == $lang) {
					$text = $t->value;
					if ($text == '') {
						$textualtown = $textual;
					}
				}
			}
			
			$font = ( string ) $textual->lang->font . '.ttf';
			imagettftext ( $im, ( int ) $textual->lang->size * 0.7, 0, ( int ) $textual->lang->position->left, ( int ) $textual->lang->position->top + ( int ) $textual->lang->size * 0.7, $color, $font, $text );
		}
		// town data
		
		if ($textualtown != '')
			$textual = $textualtown;
		else
			$textual = $subxml->town->content;
		
		$colors = rgb2array ( $textual->lang->color );
		$color = imagecolorallocate ( $im, $colors [0], $colors [1], $colors [2] );
		$text = $town;
		$font = ( string ) $textual->lang->font . '.ttf';
		$decal = 0;
		$bbox = imageftbbox ( ( int ) $textual->lang->size * 0.7, 0, $font, $text );
		$maxWidth = ( int ) $subxml->town->width;
		$currentWidth = $bbox [4] - $bbox [6];
		$decal = round ( ($maxWidth - $currentWidth) / 2, 0 );
		if ($textual->lang->center != 'true')
			$decal = 0;
		$text1 = "";
		$newsize = 0;
		$reduc = 0;
		if (($banner == 'b728090') && ($currentWidth > 90)) {
			
			if ($pos = strrpos ( $text, " " )) {
				// echo $banner." ".$currentWidth." ".$pos." ".$text;
				$text1 = substr ( $text, 0, $pos );
				$text2 = substr ( $text, $pos + 1, strlen ( $text ) - $pos );
				$text = $text1;
				$text1 = $text2;
			} else {
				$newsize = 1;
				$reduc = 2;
			}
		}
		if (($banner == 'b468060') && ($currentWidth > 50)) {
			
			if ($pos = strrpos ( $text, " " )) {
				// echo $banner." ".$currentWidth." ".$pos." ".$text;
				$text1 = substr ( $text, 0, $pos );
				$text2 = substr ( $text, $pos + 1, strlen ( $text ) - $pos );
				$text = $text1;
				$text1 = $text2;
			} else {
				$newsize = 1;
				$reduc = 3;
			}
		}
		
		$text = $townValue;
		
		// print_r( (int)$textual->lang->position->left);
		if ($newsize == 0) {
			imagettftext ( $im, ( int ) $textual->lang->size * 0.7, 0, $decal + ( int ) $textual->lang->position->left, ( int ) $textual->lang->position->top + ( int ) $textual->lang->size * 0.7, $color, $font, $text );
		} else {
			imagettftext ( $im, (( int ) $textual->lang->size - $reduc) * 0.7, 0, $decal + ( int ) $textual->lang->position->left, ( int ) $textual->lang->position->top + (( int ) $textual->lang->size - $reduc) * 0.7, $color, $font, $text );
		}
		if ($text1 != "") {
			$textual = $subxml->town->extra;
			imagettftext ( $im, ( int ) $textual->lang->size * 0.7, 0, $decal + ( int ) $textual->lang->position->left, ( int ) $textual->lang->position->top + ( int ) $textual->lang->size * 0.7, $color, $font, $text1 );
		}
		
		if (count ( $subxml->photovoltaique->children () ) > 0) {
			// photovoltaique
			$imphotovoltaique = imagecreatefrompng ( 'banners-content/images/' . $subxml->photovoltaique->image->url );
			list ( $photovoltaiquewidth, $photovoltaiqueheight, $photovoltaiquetype, $photovoltaiqueattr ) = getimagesize ( 'banners-content/images/' . $subxml->photovoltaique->image->url );
			imagecopy ( $im, $imphotovoltaique, $subxml->photovoltaique->image->position->left, $subxml->photovoltaique->image->position->top, 0, 0, $photovoltaiquewidth, $photovoltaiqueheight );
			// photovoltaique text
			foreach ( $subxml->photovoltaique->content as $textual ) {
				$colors = rgb2array ( $textual->lang->color );
				$color = imagecolorallocate ( $im, $colors [0], $colors [1], $colors [2] );
				$trads = $textual;
				$text = "";
				foreach ( $trads as $t ) {
					if (( string ) $t->attributes ()->id == $lang) {
						$text = $t->value;
					}
				}
				$font = ( string ) $textual->lang->font . '.ttf';
				
				$decal = 0;
				$bbox = imageftbbox ( ( int ) $textual->lang->size * 0.7, 0, $font, $text );
				$maxWidth = ( int ) $textual->lang->width;
				$currentWidth = $bbox [4] - $bbox [6];
				$decal = round ( ($maxWidth - $currentWidth) / 2, 0 );
				if ($textual->lang->center != 'true')
					$decal = 0;
				imagettftext ( $im, ( int ) $textual->lang->size * 0.7, 0, $decal + ( int ) $textual->lang->position->left, ( int ) $textual->lang->position->top + ( int ) $textual->lang->size * 0.7, $color, $font, $text );
			}
			// photovolta data
			$dataValue = null;
			$dataValueList = $xml2->PhotoVoltEnergy->solar_point;
			foreach ( $dataValueList as $t ) {
				if ((( string ) $t->name == $town) or (( string ) $t->name_en == $town)) {
					$dataValue = $t;
				}
			}
			
			$textual = $subxml->photovoltaique->data;
			$colors = rgb2array ( $textual->lang->color );
			$color = imagecolorallocate ( $im, $colors [0], $colors [1], $colors [2] );
			if ($period == 'daily') {
				$text = $dataValue->ElectricNeedscoverage . "%";
				if (($country == 'belgium') || ($country == 'italy') || ($country == 'slovenia') || ($country == 'sweden') || ($country == 'hungary') || ($country == 'germany') || ($country == 'spain') || ($country == 'united_kingdom'))
					$text = $dataValue->NeedscoveragePV . "%";
			} else {
				$text = $dataValue->NeedscoveragePV . "%";
			}
			$font = ( string ) $textual->lang->font . '.ttf';
			
			$decal = 0;
			$bbox = imageftbbox ( ( int ) $textual->lang->size * 0.7, 0, $font, $text );
			$maxWidth = ( int ) $textual->lang->width;
			$currentWidth = $bbox [4] - $bbox [6];
			$decal = round ( ($maxWidth - $currentWidth) / 2, 0 );
			if ($textual->lang->center != 'true')
				$decal = 0;
			imagettftext ( $im, ( int ) $textual->lang->size * 0.7, 0, $decal + ( int ) $textual->lang->position->left, ( int ) $textual->lang->position->top + ( int ) $textual->lang->size * 0.7, $color, $font, $text );
		}
		
		if (count ( $subxml->eolien->children () ) > 0) {
			// eolien
			
			$imeolien = imagecreatefrompng ( 'banners-content/images/' . $subxml->eolien->image->url );
			list ( $eolienwidth, $eolienheight, $eolientype, $eolienattr ) = getimagesize ( 'banners-content/images/' . $subxml->eolien->image->url );
			imagecopy ( $im, $imeolien, $subxml->eolien->image->position->left, $subxml->eolien->image->position->top, 0, 0, $eolienwidth, $eolienheight );
			// eolien text
			foreach ( $subxml->eolien->content as $textual ) {
				$colors = rgb2array ( $textual->lang->color );
				$color = imagecolorallocate ( $im, $colors [0], $colors [1], $colors [2] );
				$trads = $textual;
				$text = "";
				foreach ( $trads as $t ) {
					if (( string ) $t->attributes ()->id == $lang) {
						$text = $t->value;
					}
				}
				$font = ( string ) $textual->lang->font . '.ttf';
				
				$decal = 0;
				$bbox = imageftbbox ( ( int ) $textual->lang->size * 0.7, 0, $font, $text );
				$maxWidth = ( int ) $textual->lang->width;
				$currentWidth = $bbox [4] - $bbox [6];
				$decal = round ( ($maxWidth - $currentWidth) / 2, 0 );
				if ($textual->lang->center != 'true')
					$decal = 0;
				imagettftext ( $im, ( int ) $textual->lang->size * 0.7, 0, $decal + ( int ) $textual->lang->position->left, ( int ) $textual->lang->position->top + ( int ) $textual->lang->size * 0.7, $color, $font, $text );
			}
			// eolien data
			$dataValue = null;
			$dataValueList = $xml2->PhotoVoltEnergy->solar_point;
			foreach ( $dataValueList as $t ) {
				if ((( string ) $t->name == $town) or (( string ) $t->name_en == $town)) {
					$dataValue = $t;
				}
			}
			$area = $dataValue->parentwindarea;
			
			$dataValue = null;
			$dataValueList = $xml2->WindEnergy->Areas;
			
			foreach ( $dataValueList->Area as $t ) {
				if (strtolower ( ( string ) $t->name ) == strtolower ( $area )) {
					$dataValue = $t;
					break;
				}
			}
			
			$textual = $subxml->eolien->data;
			$colors = rgb2array ( $textual->lang->color );
			$color = imagecolorallocate ( $im, $colors [0], $colors [1], $colors [2] );
			$text = "Unavailable";
			if ($lang == "fr")
				$text = "Indisponible";
			if ($lang == "it")
				$text = "Non disponibile";
			if ($lang == "pt")
				$text = "Indisponível";
			$font = ( string ) $textual->lang->font . '.ttf';
			$decal = 0;
			$bbox = imageftbbox ( ( int ) $textual->lang->size * 0.7, 0, $font, $text );
			$maxWidth = ( int ) $textual->lang->width;
			$currentWidth = $bbox [4] - $bbox [6];
			$decal = round ( ($maxWidth - $currentWidth) / 2, 0 );
			if ($textual->lang->center != 'true')
				$decal = 0;
			imagettftext ( $im, ( int ) $textual->lang->size * 0.7, 0, $decal + ( int ) $textual->lang->position->left, ( int ) $textual->lang->position->top + ( int ) $textual->lang->size * 0.7, $color, $font, $text );
		}
		
		if (count ( $subxml->thermique->children () ) > 0) {
			// thermique
			$imthermique = imagecreatefrompng ( 'banners-content/images/' . $subxml->thermique->image->url );
			list ( $thermiquewidth, $thermiqueheight, $thermiquetype, $thermiqueattr ) = getimagesize ( 'banners-content/images/' . $subxml->thermique->image->url );
			imagecopy ( $im, $imthermique, $subxml->thermique->image->position->left, $subxml->thermique->image->position->top, 0, 0, $thermiquewidth, $thermiqueheight );
			// thermique text
			foreach ( $subxml->thermique->content as $textual ) {
				$colors = rgb2array ( $textual->lang->color );
				$color = imagecolorallocate ( $im, $colors [0], $colors [1], $colors [2] );
				
				$trads = $textual;
				$text = "";
				foreach ( $trads as $t ) {
					if (( string ) $t->attributes ()->id == $lang) {
						$text = $t->value;
					}
				}
				$font = ( string ) $textual->lang->font . '.ttf';
				$decal = 0;
				$bbox = imageftbbox ( ( int ) $textual->lang->size * 0.7, 0, $font, $text );
				$maxWidth = ( int ) $textual->lang->width;
				$currentWidth = $bbox [4] - $bbox [6];
				$decal = round ( ($maxWidth - $currentWidth) / 2, 0 );
				if ($textual->lang->center != 'true')
					$decal = 0;
				imagettftext ( $im, ( int ) $textual->lang->size * 0.7, 0, $decal + ( int ) $textual->lang->position->left, ( int ) $textual->lang->position->top + ( int ) $textual->lang->size * 0.7, $color, $font, $text );
			}
			// thermique data
			$dataValue = null;
			$dataValueList = $xml2->SolarTherm->solar_point;
			foreach ( $dataValueList as $t ) {
				if ((( string ) $t->name == $town) or (( string ) $t->name_en == $town)) {
					$dataValue = $t;
				}
			}
			
			$textual = $subxml->thermique->data;
			$colors = rgb2array ( $textual->lang->color );
			$color = imagecolorallocate ( $im, $colors [0], $colors [1], $colors [2] );
			
			if ($period == 'daily') {
				
				$text = $dataValue->ThermalNeedscoverage . "%";
				if (($country == 'belgium') || ($country == 'italy') || ($country == 'slovenia') || ($country == 'sweden') || ($country == 'hungary') || ($country == 'germany') || ($country == 'spain') || ($country == 'united_kingdom'))
					$text = $dataValue->NeedscoverageTH . "%";
			} else {
				$text = $dataValue->NeedscoverageTH . "%";
			}
			// $text = $dataValue->NeedscoverageTH."%";
			$font = ( string ) $textual->lang->font . '.ttf';
			
			$decal = 0;
			$bbox = imageftbbox ( ( int ) $textual->lang->size * 0.7, 0, $font, $text );
			$maxWidth = ( int ) $textual->lang->width;
			$currentWidth = $bbox [4] - $bbox [6];
			$decal = round ( ($maxWidth - $currentWidth) / 2, 0 );
			if ($textual->lang->center != 'true')
				$decal = 0;
			imagettftext ( $im, ( int ) $textual->lang->size * 0.7, 0, $decal + ( int ) $textual->lang->position->left, ( int ) $textual->lang->position->top + ( int ) $textual->lang->size * 0.7, $color, $font, $text );
		}
	} else {
		$period = $_GET ['period'];
		$town = $_GET ['town'];
		$technos = $_GET ['technos'] ? $_GET ['technos'] : 'banners';
		$towns = explode ( ',', $town );
		if (count ( $towns ) > 1) {
			$town = $towns [rand ( 0, count ( $towns ) - 1 )];
		}
		
		$banner = $_GET ['format'];
		$lang = $_GET ['lang'];
		$error = 0;
		
		$xmlBan = file_get_contents ( $technos . '.xml' );
		
		$file = 'tmp/' . $country . '_' . $period . '.xml';
		$xmlData = file_get_contents ( $file );
// 		$xmlData = file_get_contents ( 'http://ws.energizair.eu/wspartners/default?country=' . $country . '&period=' . $period . '&r=' . rand ( 1, 100000 ) );
		
		$xml = @simplexml_load_string ( $xmlBan );
		$xml2 = @simplexml_load_string ( $xmlData );
		
		// echo print_r($xml2,true);
		
		if (isset ( $xml->{$country} ))
			$subxml = $xml->{$country}->{$banner};
		else
			$subxml = $xml->default->{$banner};
			
			// $subxml=$subxml->{$banner};
			// echo ('c: '.$country.' b: '.$banner);
		$sizes = split ( 'x', $subxml->name );
		$width = $sizes [0];
		$height = $sizes [1];
		
		$background = $subxml->background->image->url;
		$im = imagecreatefrompng ( 'banners-content/images/' . $background );
		
		// logo
		$imlogo = imagecreatefrompng ( 'banners-content/images/' . $subxml->logo->image->url );
		list ( $logowidth, $logoheight, $logotype, $logoattr ) = getimagesize ( 'banners-content/images/' . $subxml->logo->image->url );
		imagecopy ( $im, $imlogo, $subxml->logo->image->position->left, $subxml->logo->image->position->top, 0, 0, $logowidth, $logoheight );
		
		// title
		if ($subxml->title->image->url) {
			$imtitle = imagecreatefrompng ( 'banners-content/images/' . $subxml->title->image->url );
			list ( $titlewidth, $titleheight, $titletype, $titleattr ) = getimagesize ( 'banners-content/images/' . $subxml->title->image->url );
			imagecopy ( $im, $imtitle, $subxml->title->image->position->left, $subxml->title->image->position->top, 0, 0, $titlewidth, $titleheight );
		}
		// title text
		foreach ( $subxml->title->content as $textual ) {
			$colors = rgb2array ( $textual->lang->color );
			$color = imagecolorallocate ( $im, $colors [0], $colors [1], $colors [2] );
			$trads = $textual;
			$text = "";
			foreach ( $trads as $t ) {
				if (( string ) $t->attributes ()->id == $lang) {
					$text = $t->value;
				}
			}
			
			$font = ( string ) $textual->lang->font . '.ttf';
			imagettftext ( $im, ( int ) $textual->lang->size * 0.7, 0, ( int ) $textual->lang->position->left, ( int ) $textual->lang->position->top + ( int ) $textual->lang->size * 0.7, $color, $font, $text );
		}
		
		// date
		$imdate = imagecreatefrompng ( 'banners-content/images/' . $subxml->date->image->url );
		list ( $datewidth, $dateheight, $datetype, $dateattr ) = getimagesize ( 'banners-content/images/' . $subxml->date->image->url );
		imagecopy ( $im, $imdate, $subxml->date->image->position->left, $subxml->date->image->position->top, 0, 0, $datewidth, $dateheight );
		// date text
		// setlocale(LC_ALL,$lang);
		if ($lang == 'pt')
			setlocale ( LC_ALL, 'pt_PT' );
		if ($lang == 'fr')
			setlocale ( LC_ALL, 'fr_FR' );
		if ($lang == 'en')
			setlocale ( LC_ALL, 'en_EN' );
		if ($lang == 'it')
			setlocale ( LC_ALL, 'it_IT' );
		if ($lang == 'sl')
			setlocale ( LC_ALL, 'sl_SI' );
		if ($lang == 'de')
			setlocale ( LC_ALL, 'de_DE' );
		if ($lang == 'hu')
			setlocale ( LC_ALL, 'hu_HU' );
		if ($lang == 'es')
			setlocale ( LC_ALL, 'es_ES' );
		if ($lang == 'ca')
			setlocale ( LC_ALL, 'es_CA' );
		if ($lang == 'sv')
			setlocale ( LC_ALL, 'sv_SV' );
		if ($lang == 'nl')
			setlocale ( LC_ALL, 'nl_NL' );
			
			// $dateDisplay=(string)$xml2->Date;
		
		$fromDateDisplay = ( string ) $xml2->fromDate;
		$toDateDisplay = ( string ) $xml2->toDate;
		
		$yearFrom = substr ( $fromDateDisplay, 0, 4 );
		$monthFrom = substr ( $fromDateDisplay, 5, 2 );
		$dayFrom = substr ( $fromDateDisplay, 8, 2 );
		
		$yearTo = substr ( $toDateDisplay, 0, 4 );
		$monthTo = substr ( $toDateDisplay, 5, 2 );
		$dayTo = substr ( $toDateDisplay, 8, 2 );
		
		$dateDisplay = ( string ) $xml2->toDate;
		$decalDate = date ( "N", strtotime ( $dateDisplay ) );
		$dateDisplay = date ( "Y-m-d", mktime ( "0", "0", "0", date ( "m", strtotime ( $dateDisplay ) ), date ( "d", strtotime ( $dateDisplay ) ) - 6, date ( "Y", strtotime ( $dateDisplay ) ) ) );
		
		$yearDisplay = substr ( $dateDisplay, 0, 4 );
		$monthDisplay = substr ( $dateDisplay, 5, 2 );
		$dayDisplay = substr ( $dateDisplay, 8, 2 );
		$startDay = $dayDisplay;
		$startDayChiffre = strtolower ( strftime ( "%d", mktime ( 0, 0, 0, 1, $dayDisplay, 1 ) ) );
		$startMonth = strtolower ( strftime ( "%b", mktime ( 0, 0, 0, $monthDisplay, 1, 1 ) ) );
		$startMonthN = strtolower ( strftime ( "%m", mktime ( 0, 0, 0, $monthDisplay, 1, 1 ) ) );
		$startMonthLong = strtolower ( strftime ( "%B", mktime ( 0, 0, 0, $monthDisplay, 1, 1 ) ) );
		$startYear = $yearDisplay;
		$endDay = date ( "d", mktime ( 0, 0, 0, $monthDisplay, $startDay + 6, $startYear ) );
		$endMonth = strtolower ( strftime ( "%b", mktime ( 0, 0, 0, $monthDisplay, $startDay + 6, $startYear ) ) );
		$endMonthN = strtolower ( strftime ( "%m", mktime ( 0, 0, 0, $monthDisplay, $startDay + 6, $startYear ) ) );
		$endMonthLong = strtolower ( strftime ( "%B", mktime ( 0, 0, 0, $monthDisplay, $startDay + 6, $startYear ) ) );
		$addon = 0;
		if ($period == "weekly") {
			// date week text
			$dateWeekText = $subxml->date->content->lang->value;
			foreach ( $subxml->date->content as $textual ) {
				$trads = $textual;
				foreach ( $trads as $t ) {
					if (( string ) $t->attributes ()->id == $lang) {
						$dateWeekText = $t->value;
					}
				}
			}
			
			if ($subxml->date->format == 'line') {
				$dateDisplay2 = $dateWeekText . " du " . $startDay . " " . $startMonth . " au " . $endDay . " " . $endMonth;
				if ($lang == 'fr') {
					$dateDisplay2 = $dateWeekText . " du " . $startDay . " " . $startMonth . " au " . $endDay . " " . $endMonth;
				}
				if ($lang == 'it') {
					$dateDisplay2 = $dateWeekText . " dal " . $startDay . " " . $startMonth . " al " . $endDay . " " . $endMonth;
				}
				if ($lang == 'sl') {
					switch ($banner) {
						case 'b120240' :
							if (isset ( $subxml->date->formatLong ) && ($subxml->date->formatLong == "true")) {
								$dateDisplay2 = $dateWeekText . " od " . $startDayChiffre . ". " . $startMonth . " do " . $endDay . ". " . $endMonth;
							} else {
								$dateDisplay2 = $dateWeekText . $startDayChiffre . ". " . $startMonth . " do " . $endDay . ". " . $endMonth;
							}
							break;
						case 'b468060' :
							if (isset ( $subxml->date->formatLong ) && ($subxml->date->formatLong == "true")) {
								$dateDisplay2 = $dateWeekText . " od " . $startDayChiffre . ". " . $startMonthLong . " do " . $endDay . ". " . $endMonthLong;
							} else {
								$dateDisplay2 = $dateWeekText . $startDayChiffre . ". " . $startMonthLong . " do " . $endDay . ". " . $endMonthLong;
							}
							break;
						case 'b160600' :
							if (isset ( $subxml->date->formatLong ) && ($subxml->date->formatLong == "true")) {
								$dateDisplay2 = $dateWeekText . " od " . $startDayChiffre . ". " . $startMonth . ($startMonth != "maj" ? "." : "") . " do " . $endDay . " " . $endMonth . "";
							} else {
								$dateDisplay2 = $dateWeekText . $startDayChiffre . ". " . $startMonth . ($startMonth != "maj" ? "." : "") . " do " . $endDay . ". " . $endMonth . ($endMonth != "maj" ? "." : "") . "";
							}
							break;
						case 'b728090' :
							if (isset ( $subxml->date->formatLong ) && ($subxml->date->formatLong == "true")) {
								$dateDisplay2 = $dateWeekText . " od " . $startDayChiffre . ". " . $startMonthLong . " do " . $endDay . ". " . $endMonthLong;
							} else {
								$dateDisplay2 = $dateWeekText . $startDayChiffre . ". " . $startMonthLong . " do " . $endDay . ". " . $endMonthLong;
							}
							break;
						case 'b125125' :
							if (isset ( $subxml->date->formatLong ) && ($subxml->date->formatLong == "true")) {
								$dateDisplay2 = $dateWeekText . " od " . $startDayChiffre . ". " . $startMonth . " do " . $endDay . ". " . $endMonth;
							} else {
								$dateDisplay2 = $dateWeekText . $startDayChiffre . ". " . $startMonth . ($startMonth != "maj" ? "." : "") . " do " . $endDay . ". " . $endMonth . ($endMonth != "maj" ? "." : "") . "";
							}
							break;
					}
				}
				if ($lang == 'pt') {
					$dateDisplay2 = $dateWeekText . " de " . $startDay . " " . $startMonth . " a " . $endDay . " " . $endMonth;
				}
				if ($lang == 'en') {
					$dateDisplay2 = $dateWeekText . " " . $startDay . " " . ucfirst ( $startMonth ) . " to " . $endDay . " " . ucfirst ( $endMonth );
				}
			} else {
				$dateDisplay2 = $startDay . " " . $startMonth;
				if ($lang == "sl") {
					$dateDisplay2 = $startDay . ". " . $startMonth . ($startMonth != "maj" ? "." : "");
				}
				$dateDisplay2b = "au";
				$dateDisplay2c = $endDay . " " . $endMonth;
				if ($lang == "sl") {
					$dateDisplay2c = $endDay . ". " . $endMonth . ($endMonth != "maj" ? "." : "");
				}
				$addon = 1;
				if ($lang == 'it') {
					$dateDisplay2b = " al ";
				}
				if ($lang == 'sl') {
					$dateDisplay2b = " do ";
				}
				if ($lang == 'pt') {
					$dateDisplay2b = " a ";
				}
				if ($lang == 'en') {
					$dateDisplay2b = " to ";
				}
				if ($lang == 'sv') {
					$dateDisplay2b = " till ";
				}
				if ($lang == 'de') {
					$dateDisplay2b = " bis ";
				}
				if ($lang == 'es') {
					$dateDisplay2b = " al ";
				}
				if ($lang == 'ca') {
					$dateDisplay2b = " al ";
				}
				if ($lang == 'nl') {
					$dateDisplay2b = " tot ";
				}
			}
		} else {
			if ($subxml->date->format == 'line') {
				$val_date_jour_1 = time () + (- 1) * 24 * 3600;
				$val_date_jour_1 = strtotime ( ( string ) $xml2->fromDate );
				if (isset ( $subxml->date->formatLong ) && ($subxml->date->formatLong == "true")) {
					$dateDisplay2 = (strftime ( "%A %d %B", $val_date_jour_1 ));
					if ($lang == 'sl') {
						$dateDisplay2 = (strftime ( "%A, %d. %B", $val_date_jour_1 ));
						if ((($banner == "b468060") || ($banner == "b728090") || ($banner == "b655090")) && ($decalDate == 4)) {
							$dateDisplay2 = (strftime ( ", %d. %B", $val_date_jour_1 ));
						}
					}
					if ($lang == 'pt') {
						$dateDisplay2 = (strftime ( "%A-feira %d %B", $val_date_jour_1 ));
					}
				} else {
					if (isset ( $subxml->date->formatLong ) && ($subxml->date->formatLong == "medium")) {
						$dateDisplay2 = (strftime ( "%d %B", $val_date_jour_1 ));
						if ($lang == 'sl') {
							$dateDisplay2 = (strftime ( "%d. %B", $val_date_jour_1 ));
						}
					} else {
						$dateDisplay2 = (strftime ( "%d %h", $val_date_jour_1 ));
						if ($lang == 'sl') {
							if (strftime ( "%h", $val_date_jour_1 ) != "maj")
								$dateDisplay2 = (strftime ( "%d. %h.", $val_date_jour_1 ));
							else
								$dateDisplay2 = (strftime ( "%d. %h", $val_date_jour_1 ));
						}
					}
				}
			} else {
				$val_date_jour_1 = time () + (- 1) * 24 * 3600;
				$val_date_jour_1 = strtotime ( ( string ) $xml2->fromDate );
				
				if (isset ( $subxml->date->formatLong ) && ($subxml->date->formatLong == "true")) {
					$dateDisplay2 = (strftime ( "%A %d %B", $val_date_jour_1 ));
					if ($lang == 'sl') {
						$dateDisplay2 = (strftime ( "%A, %d. %B", $val_date_jour_1 ));
					}
					if ($lang == 'pt') {
						$dateDisplay2 = (strftime ( "%A-feira %d %B", $val_date_jour_1 ));
					}
				} else {
					$dateDisplay2 = (strftime ( "%d %h", $val_date_jour_1 ));
					if ($lang == 'sl') {
						if (strftime ( "%h", $val_date_jour_1 ) != "maj")
							$dateDisplay2 = (strftime ( "%d. %h.", $val_date_jour_1 ));
						else
							$dateDisplay2 = (strftime ( "%d. %h", $val_date_jour_1 ));
					}
				}
				
				$dateDisplay2b = $dateDisplay2;
				$dateDisplay2 = "";
				$dateDisplay2c = "";
				$addon = 1;
			}
		}
		foreach ( $subxml->date->content as $textual ) {
			$colors = rgb2array ( $textual->lang->color );
			$color = imagecolorallocate ( $im, $colors [0], $colors [1], $colors [2] );
			$text = $dateDisplay2;
			$font = ( string ) $textual->lang->font . '.ttf';
			
			$decal = 0;
			$bbox = imageftbbox ( ( int ) $textual->lang->size * 0.7, 0, $font, $text );
			$maxWidth = ( int ) $subxml->date->width;
			$currentWidth = $bbox [4] - $bbox [6];
			$decal = round ( ($maxWidth - $currentWidth) / 2, 0 );
			if ($subxml->date->center != 'true')
				$decal = 0;
			if ((isset ( $subxml->date->formatLong ) && ($subxml->date->formatLong == "true")) && ($lang == 'sl') && ($period != 'weekly') && ((($banner == "b468060") || ($banner == "b728090") || ($banner == "b655090")) && ($decalDate == 4))) {
				if ($banner == "b468060") {
					$imdate2 = imagecreatefrompng ( 'banners-content/images/i468060-thursday-slovenia.png' );
					list ( $datewidth2, $dateheight2, $datetype2, $dateattr2 ) = getimagesize ( 'banners-content/images/i468060-thursday-slovenia.png' );
					imagecopy ( $im, $imdate2, - 17 + $decal + ( int ) $textual->lang->position->left, ( int ) $textual->lang->position->top + ( int ) $textual->lang->size * 0.7 - 8, 0, 0, $datewidth2, $dateheight2 );
					
					imagettftext ( $im, ( int ) $textual->lang->size * 0.7, 0, 17 + $decal + ( int ) $textual->lang->position->left, ( int ) $textual->lang->position->top + ( int ) $textual->lang->size * 0.7, $color, $font, $text );
				}
				if ($banner == "b728090") {
					$imdate2 = imagecreatefrompng ( 'banners-content/images/i728090-thursday-slovenia.png' );
					list ( $datewidth2, $dateheight2, $datetype2, $dateattr2 ) = getimagesize ( 'banners-content/images/i728090-thursday-slovenia.png' );
					imagecopy ( $im, $imdate2, - 26 + $decal + ( int ) $textual->lang->position->left, ( int ) $textual->lang->position->top + ( int ) $textual->lang->size * 0.7 - 11, 0, 0, $datewidth2, $dateheight2 );
					
					imagettftext ( $im, ( int ) $textual->lang->size * 0.7, 0, 25 + $decal + ( int ) $textual->lang->position->left, ( int ) $textual->lang->position->top + ( int ) $textual->lang->size * 0.7, $color, $font, $text );
				}
			} else {
				imagettftext ( $im, ( int ) $textual->lang->size * 0.7, 0, $decal + ( int ) $textual->lang->position->left, ( int ) $textual->lang->position->top + ( int ) $textual->lang->size * 0.7, $color, $font, $text );
			}
			
			if ($addon == 1) {
				$colors = rgb2array ( $textual->lang->color );
				$color = imagecolorallocate ( $im, $colors [0], $colors [1], $colors [2] );
				$text = $dateDisplay2b;
				
				$font = ( string ) $textual->lang->font . '.ttf';
				
				$decal = 0;
				$bbox = imageftbbox ( ( int ) $textual->lang->size * 0.7, 0, $font, $text );
				$maxWidth = ( int ) $subxml->date->width;
				$currentWidth = $bbox [4] - $bbox [6];
				$decal = round ( ($maxWidth - $currentWidth) / 2, 0 );
				if ($subxml->date->center != 'true')
					$decal = 0;
				
				imagettftext ( $im, ( int ) $textual->lang->size * 0.7, 0, $decal + ( int ) $textual->lang->position->left + 8, ( int ) $textual->lang->position->top + ( int ) $textual->lang->size * 1.4 + 4, $color, $font, $text );
				
				$colors = rgb2array ( $textual->lang->color );
				$color = imagecolorallocate ( $im, $colors [0], $colors [1], $colors [2] );
				$text = $dateDisplay2c;
				$font = ( string ) $textual->lang->font . '.ttf';
				
				$decal = 0;
				$bbox = imageftbbox ( ( int ) $textual->lang->size * 0.7, 0, $font, $text );
				$maxWidth = ( int ) $subxml->date->width;
				$currentWidth = $bbox [4] - $bbox [6];
				$decal = round ( ($maxWidth - $currentWidth) / 2, 0 );
				
				if ($subxml->date->center != 'true')
					$decal = 0;
				
				imagettftext ( $im, ( int ) $textual->lang->size * 0.7, 0, $decal + ( int ) $textual->lang->position->left, ( int ) $textual->lang->position->top + ( int ) $textual->lang->size * 2.1 + 8, $color, $font, $text );
			}
		}
		
		// town
		if ($subxml->town->image->url) {
			$imtown = imagecreatefrompng ( 'banners-content/images/' . $subxml->town->image->url );
			list ( $townwidth, $townheight, $towntype, $townattr ) = getimagesize ( 'banners-content/images/' . $subxml->town->image->url );
			imagecopy ( $im, $imtown, $subxml->town->image->position->left, $subxml->town->image->position->top, 0, 0, $townwidth, $townheight );
		}
		// town text
		foreach ( $subxml->town->content as $textual ) {
			$colors = rgb2array ( $textual->lang->color );
			$color = imagecolorallocate ( $im, $colors [0], $colors [1], $colors [2] );
			
			$trads = $textual;
			$text = "";
			$textualtown = "";
			foreach ( $trads as $t ) {
				if (( string ) $t->attributes ()->id == $lang) {
					$text = $t->value;
					if ($text == '') {
						$textualtown = utf8_decode ( $textual );
					}
				}
			}
			
			$font = ( string ) $textual->lang->font . '.ttf';
			imagettftext ( $im, ( int ) $textual->lang->size * 0.7, 0, ( int ) $textual->lang->position->left, ( int ) $textual->lang->position->top + ( int ) $textual->lang->size * 0.7, $color, $font, $text );
		}
		// town data
		
		if ($textualtown != '')
			$textual = $textualtown;
		else
			$textual = $subxml->town->content;
		
		$colors = rgb2array ( $textual->lang->color );
		$color = imagecolorallocate ( $im, $colors [0], $colors [1], $colors [2] );
		$text = $town;
		$font = ( string ) $textual->lang->font . '.ttf';
		$decal = 0;
		$bbox = imageftbbox ( ( int ) $textual->lang->size * 0.7, 0, $font, $text );
		$maxWidth = ( int ) $subxml->town->width;
		$currentWidth = $bbox [4] - $bbox [6];
		$decal = round ( ($maxWidth - $currentWidth) / 2, 0 );
		if ($textual->lang->center != 'true')
			$decal = 0;
		$text1 = "";
		$newsize = 0;
		$reduc = 0;
		if (($banner == 'b728090') && ($currentWidth > 90)) {
			
			if ($pos = strrpos ( $text, " " )) {
				// echo $banner." ".$currentWidth." ".$pos." ".$text;
				$text1 = substr ( $text, 0, $pos );
				$text2 = substr ( $text, $pos + 1, strlen ( $text ) - $pos );
				$text = $text1;
				$text1 = $text2;
			} else {
				$newsize = 1;
				$reduc = 2;
			}
		}
		if (($banner == 'b468060') && ($currentWidth > 50)) {
			
			if ($pos = strrpos ( $text, " " )) {
				// echo $banner." ".$currentWidth." ".$pos." ".$text;
				$text1 = substr ( $text, 0, $pos );
				$text2 = substr ( $text, $pos + 1, strlen ( $text ) - $pos );
				$text = $text1;
				$text1 = $text2;
			} else {
				$newsize = 1;
				$reduc = 3;
			}
		}
		
		$text = $townValue;
		
		// print_r( (int)$textual->lang->position->left);
		if ($newsize == 0) {
			imagettftext ( $im, ( int ) $textual->lang->size * 0.7, 0, $decal + ( int ) $textual->lang->position->left, ( int ) $textual->lang->position->top + ( int ) $textual->lang->size * 0.7, $color, $font, $text );
		} else {
			imagettftext ( $im, (( int ) $textual->lang->size - $reduc) * 0.7, 0, $decal + ( int ) $textual->lang->position->left, ( int ) $textual->lang->position->top + (( int ) $textual->lang->size - $reduc) * 0.7, $color, $font, $text );
		}
		if ($text1 != "") {
			$textual = $subxml->town->extra;
			imagettftext ( $im, ( int ) $textual->lang->size * 0.7, 0, $decal + ( int ) $textual->lang->position->left, ( int ) $textual->lang->position->top + ( int ) $textual->lang->size * 0.7, $color, $font, $text1 );
		}
		
		if (count ( $subxml->photovoltaique->children () ) > 0) {
			// photovoltaique
			$imphotovoltaique = imagecreatefrompng ( 'banners-content/images/' . $subxml->photovoltaique->image->url );
			list ( $photovoltaiquewidth, $photovoltaiqueheight, $photovoltaiquetype, $photovoltaiqueattr ) = getimagesize ( 'banners-content/images/' . $subxml->photovoltaique->image->url );
			imagecopy ( $im, $imphotovoltaique, $subxml->photovoltaique->image->position->left, $subxml->photovoltaique->image->position->top, 0, 0, $photovoltaiquewidth, $photovoltaiqueheight );
			// photovoltaique text
			foreach ( $subxml->photovoltaique->content as $textual ) {
				$colors = rgb2array ( $textual->lang->color );
				$color = imagecolorallocate ( $im, $colors [0], $colors [1], $colors [2] );
				$trads = $textual;
				$text = "";
				foreach ( $trads as $t ) {
					if (( string ) $t->attributes ()->id == $lang) {
						$text = $t->value;
					}
				}
				$font = ( string ) $textual->lang->font . '.ttf';
				
				$decal = 0;
				$bbox = imageftbbox ( ( int ) $textual->lang->size * 0.7, 0, $font, $text );
				$maxWidth = ( int ) $textual->lang->width;
				$currentWidth = $bbox [4] - $bbox [6];
				$decal = round ( ($maxWidth - $currentWidth) / 2, 0 );
				if ($textual->lang->center != 'true')
					$decal = 0;
				imagettftext ( $im, ( int ) $textual->lang->size * 0.7, 0, $decal + ( int ) $textual->lang->position->left, ( int ) $textual->lang->position->top + ( int ) $textual->lang->size * 0.7, $color, $font, $text );
			}
			// photovolta data
			$dataValue = null;
			$dataValueList = $xml2->PhotoVoltEnergy->solar_point;
			foreach ( $dataValueList as $t ) {
				if ((( string ) $t->name == $town) or (( string ) $t->name_en == $town)) {
					$dataValue = $t;
				}
			}
			
			$textual = $subxml->photovoltaique->data;
			$colors = rgb2array ( $textual->lang->color );
			$color = imagecolorallocate ( $im, $colors [0], $colors [1], $colors [2] );
			if ($period == 'daily') {
				$text = $dataValue->ElectricNeedscoverage . "%";
				if (($country == 'belgium') || ($country == 'italy') || ($country == 'slovenia') || ($country == 'sweden') || ($country == 'hungary') || ($country == 'germany') || ($country == 'spain') || ($country == 'united_kingdom'))
					$text = $dataValue->NeedscoveragePV . "%";
			} else {
				$text = $dataValue->NeedscoveragePV . "%";
			}
			$font = ( string ) $textual->lang->font . '.ttf';
			
			$decal = 0;
			$bbox = imageftbbox ( ( int ) $textual->lang->size * 0.7, 0, $font, $text );
			$maxWidth = ( int ) $textual->lang->width;
			$currentWidth = $bbox [4] - $bbox [6];
			$decal = round ( ($maxWidth - $currentWidth) / 2, 0 );
			if ($textual->lang->center != 'true')
				$decal = 0;
			imagettftext ( $im, ( int ) $textual->lang->size * 0.7, 0, $decal + ( int ) $textual->lang->position->left, ( int ) $textual->lang->position->top + ( int ) $textual->lang->size * 0.7, $color, $font, $text );
		}
		
		if (count ( $subxml->eolien->children () ) > 0) {
			// eolien
			
			$imeolien = imagecreatefrompng ( 'banners-content/images/' . $subxml->eolien->image->url );
			list ( $eolienwidth, $eolienheight, $eolientype, $eolienattr ) = getimagesize ( 'banners-content/images/' . $subxml->eolien->image->url );
			imagecopy ( $im, $imeolien, $subxml->eolien->image->position->left, $subxml->eolien->image->position->top, 0, 0, $eolienwidth, $eolienheight );
			// eolien text
			foreach ( $subxml->eolien->content as $textual ) {
				$colors = rgb2array ( $textual->lang->color );
				$color = imagecolorallocate ( $im, $colors [0], $colors [1], $colors [2] );
				$trads = $textual;
				$text = "";
				foreach ( $trads as $t ) {
					if (( string ) $t->attributes ()->id == $lang) {
						$text = $t->value;
					}
				}
				$font = ( string ) $textual->lang->font . '.ttf';
				
				$decal = 0;
				$bbox = imageftbbox ( ( int ) $textual->lang->size * 0.7, 0, $font, $text );
				$maxWidth = ( int ) $textual->lang->width;
				$currentWidth = $bbox [4] - $bbox [6];
				$decal = round ( ($maxWidth - $currentWidth) / 2, 0 );
				if ($textual->lang->center != 'true')
					$decal = 0;
				imagettftext ( $im, ( int ) $textual->lang->size * 0.7, 0, $decal + ( int ) $textual->lang->position->left, ( int ) $textual->lang->position->top + ( int ) $textual->lang->size * 0.7, $color, $font, $text );
			}
			// eolien data
			$dataValue = null;
			$dataValueList = $xml2->WindEnergy->Areas;
			
			foreach ( $dataValueList->Area as $t ) {
				if (strtolower ( ( string ) $t->name ) == $_GET ['country']) {
					$dataValue = $t;
					break;
				}
			}
			
			$textual = $subxml->eolien->data;
			$colors = rgb2array ( $textual->lang->color );
			$color = imagecolorallocate ( $im, $colors [0], $colors [1], $colors [2] );
			// die($_GET['country']." ".strtolower((string)$t->name)." ".print_r($dataValue,true)." ".print_r($dataValueList,true));
			$text = $dataValue->NumberOfHouses;
			// $text="Unavailable";
			/*
			 * if ($lang=="fr") $text = "Indisponible"; if ($lang=="it") $text="Non disponibile"; if ($lang=="pt") $text="Indisponível";
			 */
			$font = ( string ) $textual->lang->font . '.ttf';
			$decal = 0;
			$bbox = imageftbbox ( ( int ) $textual->lang->size * 0.7, 0, $font, $text );
			$maxWidth = ( int ) $textual->lang->width;
			$currentWidth = $bbox [4] - $bbox [6];
			$decal = round ( ($maxWidth - $currentWidth) / 2, 0 );
			if ($textual->lang->center != 'true')
				$decal = 0;
			imagettftext ( $im, ( int ) $textual->lang->size * 0.7, 0, $decal + ( int ) $textual->lang->position->left, ( int ) $textual->lang->position->top + ( int ) $textual->lang->size * 0.7, $color, $font, $text );
		}
		
		if (count ( $subxml->thermique->children () ) > 0) {
			// thermique
			$imthermique = imagecreatefrompng ( 'banners-content/images/' . $subxml->thermique->image->url );
			list ( $thermiquewidth, $thermiqueheight, $thermiquetype, $thermiqueattr ) = getimagesize ( 'banners-content/images/' . $subxml->thermique->image->url );
			imagecopy ( $im, $imthermique, $subxml->thermique->image->position->left, $subxml->thermique->image->position->top, 0, 0, $thermiquewidth, $thermiqueheight );
			// thermique text
			foreach ( $subxml->thermique->content as $textual ) {
				$colors = rgb2array ( $textual->lang->color );
				$color = imagecolorallocate ( $im, $colors [0], $colors [1], $colors [2] );
				$trads = $textual;
				$text = "";
				foreach ( $trads as $t ) {
					if (( string ) $t->attributes ()->id == $lang) {
						$text = $t->value;
					}
				}
				$font = ( string ) $textual->lang->font . '.ttf';
				$decal = 0;
				$bbox = imageftbbox ( ( int ) $textual->lang->size * 0.7, 0, $font, $text );
				$maxWidth = ( int ) $textual->lang->width;
				$currentWidth = $bbox [4] - $bbox [6];
				$decal = round ( ($maxWidth - $currentWidth) / 2, 0 );
				if ($textual->lang->center != 'true')
					$decal = 0;
				imagettftext ( $im, ( int ) $textual->lang->size * 0.7, 0, $decal + ( int ) $textual->lang->position->left, ( int ) $textual->lang->position->top + ( int ) $textual->lang->size * 0.7, $color, $font, $text );
			}
			// thermique data
			$dataValue = null;
			$dataValueList = $xml2->SolarTherm->solar_point;
			foreach ( $dataValueList as $t ) {
				if ((( string ) $t->name == $town) or (( string ) $t->name_en == $town)) {
					$dataValue = $t;
				}
			}
			
			$textual = $subxml->thermique->data;
			$colors = rgb2array ( $textual->lang->color );
			$color = imagecolorallocate ( $im, $colors [0], $colors [1], $colors [2] );
			
			if ($period == 'daily') {
				
				$text = $dataValue->ThermalNeedscoverage . "%";
				if (($country == 'belgium') || ($country == 'italy') || ($country == 'slovenia') || ($country == 'sweden') || ($country == 'hungary') || ($country == 'germany') || ($country == 'spain') || ($country == 'united_kingdom'))
					$text = $dataValue->NeedscoverageTH . "%";
			} else {
				$text = $dataValue->NeedscoverageTH . "%";
			}
			// $text = $dataValue->NeedscoverageTH."%";
			$font = ( string ) $textual->lang->font . '.ttf';
			
			$decal = 0;
			$bbox = imageftbbox ( ( int ) $textual->lang->size * 0.7, 0, $font, $text );
			$maxWidth = ( int ) $textual->lang->width;
			$currentWidth = $bbox [4] - $bbox [6];
			$decal = round ( ($maxWidth - $currentWidth) / 2, 0 );
			if ($textual->lang->center != 'true')
				$decal = 0;
			imagettftext ( $im, ( int ) $textual->lang->size * 0.7, 0, $decal + ( int ) $textual->lang->position->left, ( int ) $textual->lang->position->top + ( int ) $textual->lang->size * 0.7, $color, $font, $text );
		}
	}
	
	imagepng ( $im );
	imagedestroy ( $im );
}
?>