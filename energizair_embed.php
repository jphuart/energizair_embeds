<html>
<head>
<meta charset="UTF-8">
<title>EnergizAIR - Embed</title>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="./jquery.js"><\/script>')</script>
<script src="./slide.js"></script>
</head>
<body>

	<div id="embed">
		<?php
		$country = $_GET ['country'];
		$period = $_GET ['period'];
		
		$countrytest = array (
				'belgium',
				'france',
				'italy',
				'portugal',
				'slovenia',
				'germany',
				'hungary',
				'spain',
				'sweden',
				'united_kingdom' 
		);
		if (! in_array ( $country, $countrytest )) {
			$country = 'belgium';
		}
		$periodtest = array (
				'daily',
				'weekly' 
		);
		if (! in_array ( $period, $periodtest )) {
			$period = 'weekly';
		}
		
		$urlData = 'http://ws.energizair.eu/wspartners/default?country=' . $country . '&period=' . $period . '&r=' . rand ( 1, 100000 );
		$file = 'tmp/' . $country . '_' . $period . '.xml';
		/*
		 * check if the existing file is obsolete
		 */
		$md5Check = md5_file ( $file );
		if (md5_file ( $urlData ) != $md5Check) {
			/*
			 * écrase le fichier existant par le nouveau
			 */
			$xmlData = file_get_contents ( $urlData );
			$test = file_put_contents ( $file, $xmlData );
		}
		
		$url = "http://www.energizair.eu";
		if ($country == 'belgium')
			$url = "http://www.meteo-renouvelable.be";
		else if ($country == 'france')
			$url = "http://www.meteo-renouvelable.fr";
		else if ($country == 'italy')
			$url = "http://www.meteorinnovabili.it";
		else if ($country == 'portugal')
			$url = "http://energizair.apren.pt";
		else if ($country == 'slovenia')
			$url = "http://www.preklopinasonce.si";
		else if ($country == 'germany')
			$url = "http://www.energie-wetter.de";
		else if ($country == 'hungary')
			$url = "http://www.legbolkapottenergia.hu";
		else if ($country == 'spain')
			$url = "http://www.meteo-renovables.es";
		else if ($country == 'sweden')
			$url = "http://www.energizair.se";
		else if ($country == 'united_kingdom')
			$url = "http://www.weatherenergy.co.uk";
		
		$townArray = explode ( ',', $_GET ['townArray'] );
		for($i = 0; $i < count ( $townArray ); $i ++) {
			if ($townArray [$i] != '') {
				$dataUrl = 'http://www.energizair.eu/embed/build_trad.php?lang=' . $_GET ['lang'] . '&country=' . $country . '&period=' . $period . '&town=' . urldecode ( $townArray [$i] ) . '&format=' . $_GET ['format'] . '&technos=' . $_GET ['technos'] . '&r=' . $_GET ['r'];
				echo '<a href="' . $url . '" target="_blank" style="display:inline-block;"><img src="' . $dataUrl . '" alt="" border="0" title=""/></a>';
			}
		}
		?>
	</div>

	<script>
$(window).load(function() {
	$("#embed").carouFredSel({items:1,direction:"left",scroll:{items:1,easing:"swing",duration:1000,pauseOnHover:true}});
});
</script>
</body>
</html>

