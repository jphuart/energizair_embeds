// JavaScript Document
//require JQUERY 1.4.4 minimum
//utilisation: initSelect('myselectname');  where myselectname is the ID of the select field to change.



function activate(e, o) {
  if ($(o).attr('mystate') == 'closed') {
    $('.select').css({
      zIndex: 2
    });
    $(o).css({
      zIndex: 10000
    });
    $('.select[mystate="opened"]').click();

    $(o).animate({
      height: (2 + parseInt($(o).attr('myheight')) + $(o).find('div div').length * $(o).find('div div').height()) + 'px'
    }, 200, function () {
      $(this).attr('mystate', 'opened');
    });
  }

  if ($(o).attr('mystate') == 'opened') {
    $(o).animate({
      height: $(o).attr('myheight')
    }, 200, function () {
      $(this).attr('mystate', 'closed');
    });
  }
}

function initSelect(name) {
  var html = '';
  html += '<span>' + $('#' + name + ' option:selected').html() + '</span>';
  html += '<div class="options">'
  $('#' + name + ' option').each(function (i) {
    html += '<div class="c' + $(this).attr('value') + '" myvalue="' + $(this).attr('value') + '">' + $(this).html() + '</div>';
  });
  html += '</div>';
  $('#' + name + 'Alt').html(html);
  $('#' + name + 'Alt').attr('mystate', 'closed');
  $('#' + name + 'Alt').attr('myheight', $('#' + name + 'Alt').height());

  $('#' + name + 'Alt').unbind('click');
  $('#' + name + 'Alt').click(function (e) {
    activate(e, this);
  });

  $('#' + name + 'Alt').unbind('mouseleave');
  $('#' + name + 'Alt').mouseleave(function (e) {
    $('.select[mystate="opened"]').click();
  });

  $('#' + name + 'Alt div div').unbind('click');
  $('#' + name + 'Alt div div').click(function (e) {
    if ($(this).parent().parent().attr('mystate') == 'opened') {

      $(this).parent().parent().find('span').html($(e.currentTarget).html());
      var selectName = $(this).parent().parent().attr('id');

      selectName = selectName.substr(0, selectName.length - 3);
      $('#' + selectName + ' option:selected').removeAttr('selected');
      $('#' + selectName + ' option[value="' + $(e.currentTarget).attr('myvalue') + '"]').attr('selected', 'selected');

      //alert(document.getElementById(selectName).selectedIndex);
      $('#' + selectName).val($('#' + selectName + ' option:selected').attr('value'));
      //alert(document.getElementById(selectName).selectedIndex);
      document.getElementById(selectName).selectedIndex = $(this).index();
      //alert(document.getElementById(selectName).selectedIndex);
      $('#' + selectName).trigger('change');


    }
  });
}