<?php
$onlyBanner = $_GET['banner'];
$onlyFormat = $_GET['format'];
$onlyCountry = $_GET['country'];


$countries = array(
    'default' => array('fr'),
    'belgium' => array('fr', 'nl'),
    'italy' => array('it'),
    'slovenia' => array('sl'),
    'portugal' => array('pt'),
    'germany' => array('de'),
    'hungary' => array('hu'),
    'spain' => array('es', 'ca'),
    'sweden' => array('sv'),
    'united_kingdom' => array('en')
);

$standarFormats = array('b120240', 'b468060', 'b160600', 'b655090', 'b728090');

$banners = array(
    'banners' => $standarFormats,
    'banners-pheo' => array_merge($standarFormats, array('b120600')),
    'banners-phth' => array_merge($standarFormats, array('b120600')),
    'banners-theo' => array_merge($standarFormats, array('b120600')),
    'banners-ph' => array('b125125'),
    'banners-th' => array('b125125'),
    'banners-eo' => array('b125125'),
);

foreach ($banners as $banner => $formats) {
  if (!$onlyBanner || $onlyBanner === $banner) {
    foreach ($formats as $format) {
      if (!$onlyFormat || $onlyFormat === $format) {
        foreach ($countries as $country => $languages) {
          if (!$onlyCountry || $onlyCountry === $country) {
            foreach ($languages as $language) {
              echo "<h2>Formating. Banner: $banner Format: $format Country: $country Language: $language</h2>";
              $rand = rand(1, 10000);
              echo "<img src='http://energizair.clients.lh/build_trad.php?lang=$language&country=$country&period=weekly&town=&format=$format&technos=$banner&r=$rand'><br/>";
            }
          }
        }
      }
    }
  }
}

?>